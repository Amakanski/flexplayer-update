#!/bin/bash

# Startup scripts for the Flexplayer
# - Hide any output
# - Delete previous update download folder
# - Download latest from repo
# - Start deploy

# Disable blinker
# echo -e "\033[?25l"

# Allow 30 seconds to wait for internet to come up
TIMEOUT=30

# Returns false if site is up
check_network() {
    ping -q -w 1 -c 1 www.optiektv.nl > /dev/null 2>&1 && return 1 || return 0;
}

# Waiting for network...
while check_network
do
    sleep 1
    TIMEOUT=$[$TIMEOUT-1]
    if [ $TIMEOUT -eq "0" ]
	then
	    echo Exiting due to timeout
	    exit
    fi
done

echo Running update

# Get rid of old update files
rm -rf flexplayer-update/

# Download latest
git clone -q http://www.bitbucket.org/Amakanski/flexplayer-update.git > /dev/null

# Execute deployment script
/bin/bash flexplayer-update/deploy.sh

# Remove update files
rm -rf flexplayer-update/

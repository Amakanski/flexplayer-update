var nice_duration = function(t){
    var m = parseInt(t / 60), s = parseInt(t % 60);
    return (m > 0?m + 'm ':'') + s + 's'; 
}

var nice_filesize = function(t){
    if(t > 1048576) return parseInt(t / 1048576) + ' Mb';
    else if(t > 1024) return parseInt(t / 1024) + ' Kb';
    else return t + ' b';
}
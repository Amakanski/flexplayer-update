var interval = 1000;

setInterval(function(){

    $.get('/status/', function(response){

        
        // $('#debug').html( progress );

        Object.keys(response).forEach(function(key){
            $('[name='+ key +']').html(response[key]);
        });

        $('[name=duration_duration]').html(nice_duration(0.001 * (response.timestamp - response.playing_since) ) + ' / ' + nice_duration(response['duration']));                 
        
        if(response.duration > 0){

            var progress = 0.1 * (response.timestamp - response.playing_since) / response.duration;

            if(progress > 100)
                progress = 100;

            $('[name=play_progress]')
                .finish()
                .animate({"width": progress + '%'}, interval - 1, "linear");

        }

        if(response.download_size - response.download_progress > 0){
            $('[name=download_box]').show();

            $('[name=dl_progress]')
                .finish()
                .animate({"width": (100 * (response.download_progress / response.download_size)) + '%'}, interval - 1, "linear");

            $('[name=dl_size]')
                .html(nice_filesize(response.download_size - response.download_progress));
        }

        else $('[name=download_box]').hide();

    });

}, interval);
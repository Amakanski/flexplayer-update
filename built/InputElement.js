"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InterfaceElement_1 = require('./InterfaceElement');
var InputElement = (function (_super) {
    __extends(InputElement, _super);
    function InputElement(id) {
        _super.call(this, id);
        this.on = function (trigger, data) {
            if (trigger == 'on_paint')
                this.paint();
        };
        this.paint = function () {
        };
    }
    InputElement.prototype.setValue = function (new_value) {
        this.value = new_value;
        this.emit('on_change', { new_value: new_value });
    };
    return InputElement;
}(InterfaceElement_1["default"]));
//# sourceMappingURL=InputElement.js.map
"use strict";
var Draw = (function () {
    function Draw() {
    }
    Draw.clear = function () {
        process.stdout.write("\033[2J\033[;H");
    };
    Draw.showCursor = function (bool) {
        if (bool)
            process.stdout.write("\033[?25h");
        else
            process.stdout.write("\033[?25l");
    };
    Draw.setCursor = function (x, y) {
        process.stdout.write("\033[" + y + ";" + x + "H");
    };
    Draw.setColor = function (f, b, intense) {
        if (intense)
            process.stdout.write("\x1b[1m");
        else
            process.stdout.write("\x1b[2m");
        process.stdout.write("\x1b[" + (30 + f) + ";" + (40 + b) + "m");
    };
    Draw.setRGB = function (foreground, background) {
        if (foreground)
            process.stdout.write("\x1b[38;2;" + foreground[0] + ";" + foreground[1] + ";" + foreground[2] + "m");
        if (background)
            process.stdout.write("\x1b[48;2;" + background[0] + ";" + background[1] + ";" + background[2] + "m");
    };
    Draw.paintBox = function (box) {
        var left = box[0];
        var top = box[1];
        var width = box[2];
        var height = box[3];
        for (var x = 0; x <= width; x++)
            for (var y = 0; y <= height; y++) {
                this.setCursor(left + x, top + y);
                if (x == 0 && y == 0)
                    this.paintText(Draw.BOX.lefttop);
                else if (x == width && y == 0)
                    this.paintText(Draw.BOX.righttop);
                else if (x == 0 && y == height)
                    this.paintText(Draw.BOX.leftbottom);
                else if (x == width && y == height)
                    this.paintText(Draw.BOX.rightbottom);
                else if (y == 0)
                    this.paintText(Draw.BOX.top);
                else if (y == height)
                    this.paintText(Draw.BOX.bottom);
                else if (x == 0)
                    this.paintText(Draw.BOX.left);
                else if (x == width)
                    this.paintText(Draw.BOX.right);
                else
                    this.paintText(' ');
            }
    };
    Draw.paintText = function (str) {
        process.stdout.write(str);
    };
    Draw.use_rgb = false;
    Draw.colors = {
        "black": 0,
        "red": 1,
        "green": 2,
        "yellow": 3,
        "blue": 4,
        "magenta": 5,
        "cyan": 6,
        "white": 7
    };
    Draw.BOX = {
        "lefttop": String.fromCharCode(0x250c),
        "top": String.fromCharCode(0x2500),
        "righttop": String.fromCharCode(0x2510),
        "left": String.fromCharCode(0x2502),
        "right": String.fromCharCode(0x2502),
        "leftbottom": String.fromCharCode(0x2514),
        "bottom": String.fromCharCode(0x2500),
        "rightbottom": String.fromCharCode(0x2518)
    };
    Draw.padString = function (str, length, mask) {
        var result = str.substring(0, length);
        while (result.length < length)
            result += " ";
        if (mask && mask != '')
            for (var i = 0; i < result.length; i++)
                if (result[i] != ' ')
                    result[i] = mask[0];
        return result;
    };
    return Draw;
}());
exports.__esModule = true;
exports["default"] = Draw;
//# sourceMappingURL=Draw.js.map
"use strict";
var child_process = require('child_process');
var Promise = require('promise');
var DiskManager = (function () {
    function DiskManager() {
    }
    DiskManager.get_disk_usage = function () {
        return new Promise(function (fulfill, reject) {
            child_process.exec('df', function (error, stdout, stderr) {
                var lines = stdout.split("\n");
                var root = lines[1];
                var values = root.split(/\s/).filter(function (v) { return v != ''; });
                DiskManager.storage_total = 1000 * parseInt(values[1]);
                DiskManager.storage_used = 1000 * parseInt(values[2]);
                DiskManager.storage_free = 1000 * parseInt(values[3]);
                fulfill();
            });
        });
    };
    return DiskManager;
}());
exports.__esModule = true;
exports["default"] = DiskManager;
//# sourceMappingURL=DiskManager.js.map
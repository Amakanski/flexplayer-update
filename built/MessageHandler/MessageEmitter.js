"use strict";
var MessageEmitter = (function () {
    function MessageEmitter() {
    }
    MessageEmitter.addListener = function (listener) {
        this.listeners.push(listener);
    };
    MessageEmitter.emit = function (event, data) {
        this.listeners.forEach(function (listener) {
            listener.on(event, data);
        });
    };
    return MessageEmitter;
}());
exports.__esModule = true;
exports["default"] = MessageEmitter;
//# sourceMappingURL=MessageEmitter.js.map
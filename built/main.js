"use strict";
var MessageEmitter_1 = require('./Message/MessageEmitter');
var DownloadManager_1 = require('./Download/DownloadManager');
var PlayerLogic_1 = require('./Main/PlayerLogic');
var DownloadLogic_1 = require('./Main/DownloadLogic');
var VerboseLogic_1 = require('./Main/VerboseLogic');
var RenderLogic_1 = require('./Main/RenderLogic');
var RemoteLogic_1 = require('./Main/RemoteLogic');
var WebLogic_1 = require('./Main/WebLogic');
var Download_1 = require('./Download/Download');
var Download_2 = require('./Download/Download');
var Player_1 = require('./Player/Player');
var Device_1 = require('./Player/Device');
var Draw_1 = require('./Render/Draw');
var fs = require('fs');
var Promise = require('promise');
var async = require('async');
var settings = require('../settings.json');
var NetworkHandler_1 = require('./Network/NetworkHandler');
var DiskManager_1 = require('./Disk/DiskManager');
DiskManager_1["default"].get_disk_usage();
var debug = function (message) {
    if (settings.debug)
        console.log(message);
};
debug('Flexplayer');
debug('(c)2016-17 Anyflex');
debug('Author: Ivo Wams');
debug('');
var player;
var auth_key;
function get_config() {
    debug('Getting config...');
    return new Promise(function (fulfill, reject) {
        NetworkHandler_1["default"].getConfig(NetworkHandler_1["default"].nic_eth0, function () {
            fulfill();
        });
    });
}
function read_auth_key() {
    var auth_key_obj = JSON.parse(fs.readFileSync(settings.cache_folder + 'auth_key', 'utf8'));
    return auth_key_obj.auth_key ? auth_key_obj.auth_key : false;
}
function get_auth_key() {
    debug('Getting auth key...');
    return new Promise(function (fulfill, reject) {
        try {
            var auth_key = read_auth_key();
            if (auth_key)
                fulfill(auth_key);
            else {
                fs.unlink(settings.cache_folder + 'auth_key');
                reject('Invalid auth key');
            }
        }
        catch (error) {
            debug('Registering device...');
            var dl = new Download_1["default"]('http://' + settings.server + '/data/device/register/' + NetworkHandler_1["default"].nic_eth0.mac + '/', settings.cache_folder + 'auth_key');
            dl.on_success = function () {
                try {
                    var auth_key = read_auth_key();
                    if (auth_key)
                        fulfill(auth_key);
                    else {
                        fs.unlink(settings.cache_folder + 'auth_key');
                        reject('Unable to retrieve auth key');
                    }
                }
                catch (e) {
                    reject(e);
                }
            };
            dl.on_error = function (error) {
                reject('Download error: ' + error);
            };
            DownloadManager_1["default"].download(dl);
        }
    });
}
function register_player() {
}
function get_device() {
    debug('Getting device...');
    return new Promise(function (fulfill, reject) {
        var finalize = function () {
            try {
                var data = fs.readFileSync(settings.cache_folder + settings.device_file, 'utf8');
                var obj = JSON.parse(data);
                var device = new Device_1["default"](obj);
                fulfill(device);
            }
            catch (error) {
                reject(error);
            }
        };
        if (!fs.existsSync(settings.cache_folder + settings.device_file)) {
            debug('Getting device file');
            var dl = new Download_1["default"]('http://' + settings.server + '/data/device/status/' + auth_key + '/', settings.cache_folder + settings.device_file);
            dl.on_success = function () {
                finalize();
            };
            dl.on_error = function (error) {
                reject('Download error: ' + error);
            };
            DownloadManager_1["default"].download(dl);
        }
        else
            finalize();
    });
}
get_config()
    .then(get_auth_key)
    .then(function (ak) {
    auth_key = ak;
    debug('Auth key: ' + auth_key);
    return get_device();
})
    .then(function (device) {
    player = new Player_1["default"](device);
    DiskManager_1["default"].get_disk_usage();
})
    .then(function () {
    player.device.storage = DiskManager_1["default"].storage_total;
    player.device.storage_free = DiskManager_1["default"].storage_free;
    DownloadManager_1["default"].post_player_status(player);
    Draw_1["default"].clear();
    Draw_1["default"].showCursor(false);
    var player_logic = new PlayerLogic_1["default"](player);
    var download_logic = new DownloadLogic_1["default"](player);
    var remote_logic = new RemoteLogic_1["default"](player);
    if (settings.debug) {
        var verbose_logic = new VerboseLogic_1["default"](player);
        var render_logic = new RenderLogic_1["default"](player);
    }
    var web_logic = new WebLogic_1["default"](player);
    var playlist_download_interval = 10000;
    if (settings.playlist_download_interval)
        playlist_download_interval = settings.playlist_download_interval;
    MessageEmitter_1["default"].emit('main', 'on_initialize');
    setInterval(function () {
        DownloadManager_1["default"].download(new Download_2.PlaylistDownload(player.device));
    }, playlist_download_interval);
})
    .catch(function (error) {
    console.log("Could not start");
    console.log(error);
    if (error.stack)
        console.log(error.stack);
    Draw_1["default"].showCursor(true);
});
//# sourceMappingURL=main.js.map
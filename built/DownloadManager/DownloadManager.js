"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MessageEmitter_1 = require('../MessageHandler/MessageEmitter');
var fs = require('fs');
var DownloadManager = (function (_super) {
    __extends(DownloadManager, _super);
    function DownloadManager() {
        _super.call(this);
    }
    DownloadManager.addDownload = function (download) {
        this.downloads.push(download);
    };
    DownloadManager.startDownload = function (download) {
        DownloadManager.downloading = download;
    };
    DownloadManager.prototype.start = function () {
        this.
        ;
    };
    DownloadManager.prototype.on = function (event, data) {
        switch (event) {
        }
    };
    return DownloadManager;
}(MessageEmitter_1["default"]));
exports.__esModule = true;
exports["default"] = DownloadManager;
//# sourceMappingURL=DownloadManager.js.map
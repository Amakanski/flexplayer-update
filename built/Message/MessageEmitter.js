"use strict";
var MessageEmitter = (function () {
    function MessageEmitter() {
    }
    MessageEmitter.addListener = function (listener) {
        this.listeners.push(listener);
    };
    MessageEmitter.emit = function (source, event, data) {
        var self = this;
        setTimeout(function () {
            self.listeners.forEach(function (listener) {
                listener.on(source, event, data);
            });
        }, 0);
    };
    MessageEmitter.listeners = Array();
    return MessageEmitter;
}());
exports.__esModule = true;
exports["default"] = MessageEmitter;
//# sourceMappingURL=MessageEmitter.js.map
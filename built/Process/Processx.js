"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var ChildProcess = require('child_process');
var MessageEmitter_1 = require('../Message/MessageEmitter');
var Process = (function () {
    function Process(cmd) {
        this.pid = 0;
        this.status = '';
        this.is_playlist_item = false;
        this.cmd = cmd;
    }
    Process.prototype.start = function () {
        var self = this;
        self.status = 'starting';
        self.process = ChildProcess.exec(self.cmd, function (error, stdout, stderr) {
            self.status = 'running';
            self.stdout = '';
            self.stderr = '';
            self.pid = self.process.pid;
            if (stdout && stdout != '') {
                self.stdout += stdout;
                MessageEmitter_1["default"].emit('process::stdout', { process: self, stdout: stdout });
            }
            if (stderr && stderr != '') {
                self.stderr += stderr;
                MessageEmitter_1["default"].emit('process::stderr', { process: self, stderr: stderr });
            }
            if (error)
                MessageEmitter_1["default"].emit('process::error', { process: self, error: error });
        });
        self.process.on('exit', function () {
            self.status = 'finished';
            MessageEmitter_1["default"].emit('process::exit', self);
            if (self.callback)
                self.callback();
        });
    };
    Process.prototype.stop = function () {
        MessageEmitter_1["default"].emit('debug', 'Killing: ' + this.cmd);
        ChildProcess.exec('sh ./stop.sh');
    };
    return Process;
}());
exports.__esModule = true;
exports["default"] = Process;
var MovieProcess = (function (_super) {
    __extends(MovieProcess, _super);
    function MovieProcess(content_id) {
        var cmd = 'sh ./play_movie.sh ' + content_id;
        _super.call(this, cmd);
        this.is_playlist_item = true;
    }
    MovieProcess.prototype.stop = function () {
        _super.prototype.stop.call(this);
    };
    return MovieProcess;
}(Process));
exports.MovieProcess = MovieProcess;
var ImageProcess = (function (_super) {
    __extends(ImageProcess, _super);
    function ImageProcess(content_id, duration) {
        if (!duration)
            duration = 10;
        var cmd = 'sh ./play_image.sh ' + content_id + ' ' + duration;
        _super.call(this, cmd);
        this.is_playlist_item = true;
    }
    ImageProcess.prototype.stop = function () {
    };
    return ImageProcess;
}(Process));
exports.ImageProcess = ImageProcess;
//# sourceMappingURL=Processx.js.map
"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var ProcessManager = (function () {
    function ProcessManager() {
    }
    ProcessManager.start = function (p) {
        this.processes.push(p);
        MessageEmitter_1["default"].emit('processes::start', p);
        p.start();
    };
    ProcessManager.stop = function (p) {
        var index = ProcessManager.processes.indexOf(p);
        if (index == -1)
            return false;
        ProcessManager.processes.splice(index, 1);
        MessageEmitter_1["default"].emit('processes::stop', p);
        return true;
    };
    ProcessManager.processes = Array();
    return ProcessManager;
}());
exports.__esModule = true;
exports["default"] = ProcessManager;
//# sourceMappingURL=ProcessManager.js.map
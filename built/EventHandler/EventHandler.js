"use strict";
var EventHandler = (function () {
    function EventHandler() {
    }
    EventHandler.prototype.addListener = function (listener) {
        this.listeners.push(listener);
    };
    EventHandler.prototype.emit = function (event, data) {
        this.listeners.forEach(function (listener) {
            listener.on(event, data);
        });
    };
    return EventHandler;
}());
exports.__esModule = true;
exports["default"] = EventHandler;
//# sourceMappingURL=EventHandler.js.map
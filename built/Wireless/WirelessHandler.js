"use strict";
var child_process = require('child_process');
var util = require('util');
var fs = require('fs');
var WirelessHandler = (function () {
    function WirelessHandler() {
    }
    WirelessHandler.scan = function (callback) {
        if (this.simulation) {
            callback.on_success([
                {
                    'signal': '-20',
                    'SSID': 'Anyflex'
                },
                {
                    'signal': '-45',
                    'SSID': 'Een andere SSID'
                }
            ]);
            return true;
        }
        var scan_spawn = child_process.spawn('iw', ['dev', this.WLAN_DEVICE, 'scan']);
        var scan_data;
        var list = Array();
        scan_spawn.stdout.on('data', function (data) {
            scan_data += data;
        });
        scan_spawn.on('error', function (error) {
            callback.on_error(error);
        });
        scan_spawn.on('close', function () {
            var regex = /signal: ([-\d\.]+).*\n\slast seen: ([\d]+).*\n.*\n\sSSID: ([^\n]+)/g;
            var matches;
            var count = 0;
            while ((matches = regex.exec(scan_data)) != null)
                list.push({
                    'signal': matches[1],
                    'SSID': matches[3]
                });
            callback.on_success(list);
        });
    };
    WirelessHandler.get_connection_info = function (callback) {
        var connection_info_spawn = child_process.spawn('iw', ['dev', this.WLAN_DEVICE, 'link']);
        var connection_info;
        connection_info_spawn.stdout.on('data', function (data) {
            connection_info += data;
        });
        connection_info_spawn.on('error', function (error) {
            callback.on_error(error);
        });
        connection_info_spawn.on('close', function () {
            var regex = /SSID: ([^\n]+)/;
            var match = String(connection_info).match(regex);
            if (match != null)
                callback.on_success(match[1]);
            else
                callback.on_error('Not connected');
        });
    };
    WirelessHandler.connect = function (ssid, passcode, callback) {
        var _this = this;
        var config_spawn = child_process.spawn('wpa_passphrase', ['"' + ssid + '"', '"' + passcode + '"']);
        var config;
        callback.on_status('Creating config file');
        config_spawn.stdout.on('data', function (data) {
            config += data;
        });
        config_spawn.on('error', function (error) {
            callback.on_error(error);
        });
        config_spawn.on('close', function () {
            var self = _this;
            var regex = /\spsk=([^\n]+)/;
            var match = String(config).match(regex);
            if (match != null) {
                var psk = match[1];
                callback.on_status('Passphrase: ' + psk);
                var config_file = ssid + '.conf';
                var config_file_string = util.format(self.config_file_template, ssid, passcode);
                callback.on_status('Wrinting config file');
                fs.writeFile(config_file, config_file_string, function (err) {
                    if (err)
                        callback.on_error('Unable to write config file');
                    else {
                        callback.on_status('Initializing wpa_supplicant');
                        var wpa_supplicant = child_process.spawn('wpa_supplicant', ['-i' + self.WLAN_DEVICE, '-c' + config_file, '-B'], { detached: true });
                        var response;
                        wpa_supplicant.stdout.on('data', function (data) {
                            response += data;
                        });
                        wpa_supplicant.on('error', function (error) {
                            callback.on_error(error);
                        });
                        wpa_supplicant.on('close', function () {
                            var ip_spawn = child_process.spawn('dhclient', [self.WLAN_DEVICE]);
                            var ip_spawn_response;
                            callback.on_status('Retrieving IP address');
                            ip_spawn.stdout.on('data', function (data) {
                                ip_spawn_response += data;
                            });
                            ip_spawn.on('error', function (error) {
                                callback.on_error(error);
                            });
                            ip_spawn.on('close', function () {
                                callback.on_success();
                            });
                            callback.on_success(response);
                        });
                    }
                });
            }
            else
                callback.on_error('Unable to parse PSK');
        });
    };
    WirelessHandler.disconnect = function (callback) {
        var kill_spawn = child_process.spawn('killall', ['wpa_supplicant']);
        var response;
        kill_spawn.stdout.on('data', function (data) {
            response += data;
        });
        kill_spawn.on('error', function (error) {
            callback.on_error(error);
        });
        kill_spawn.on('close', function () {
            callback.on_success(response);
        });
    };
    WirelessHandler.WLAN_DEVICE = 'wlan0';
    WirelessHandler.simulation = true;
    WirelessHandler.config_file_template = "\
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n\
update_config=1\n\
\n\
network={\n\
ssid=\"%s\"\n\
psk=\"%s\"\n\
proto=RSN\n\
key_mgmt=WPA-PSK\n\
pairwise=CCMP\n\
auth_alg=OPEN\n\
}\
\
";
    return WirelessHandler;
}());
exports.__esModule = true;
exports["default"] = WirelessHandler;
//# sourceMappingURL=WirelessHandler.js.map
"use strict";
var Vector = (function () {
    function Vector(x, y) {
        this.x = x;
        this.y = y;
    }
    Vector.prototype.add = function (v) {
        return new Vector(this.x + v.x, this.y + v.y);
    };
    return Vector;
}());
exports.__esModule = true;
exports["default"] = Vector;
//# sourceMappingURL=Vector.js.map
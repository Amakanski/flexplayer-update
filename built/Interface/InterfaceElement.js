"use strict";
var Vector_1 = require('./Vector');
var MessageEmitter_1 = require('../Message/MessageEmitter');
var EventHandler_1 = require('./EventHandler');
var InterfaceElement = (function () {
    function InterfaceElement(json) {
        var _this = this;
        this.type = 'InterfaceElement';
        this.id = '';
        this.parent = null;
        this.parent_index = 0;
        this.children = Array();
        this.selectable = false;
        this.focus = false;
        this.display = false;
        this.readonly = false;
        this.value = '';
        this.event_handler = new EventHandler_1["default"]();
        this.id = json.id;
        this.parent = null;
        this.position = new Vector_1["default"](json.position ? json.position.left : 0, json.position ? json.position.top : 0);
        this.size = new Vector_1["default"](json.size ? json.size.width : 0, json.size ? json.size.height : 0);
        this.selectable = false;
        this.focus = false;
        this.display = json.display == 'true' || !json.display;
        this.readonly = json.readonly == "true";
        this.value = json.value;
        this.event_handler = new EventHandler_1["default"]();
        var self = this;
        this.event_handler.addListener({
            "on_focus": function () {
                self.focus = true;
                _this.change();
            },
            "on_blur": function () {
                self.focus = false;
                _this.change();
            }
        });
    }
    InterfaceElement.prototype.change = function () {
        MessageEmitter_1["default"].emit('interface', 'paint', this);
        this.event_handler.trigger('on_change', this);
    };
    InterfaceElement.prototype.show = function () {
        this.display = true;
        this.change();
    };
    InterfaceElement.prototype.hide = function () {
        this.display = false;
        this.change();
    };
    InterfaceElement.prototype.addChild = function (child) {
        child.parent = this;
        this.children.push(child);
        child.parent_index = this.children.length;
        this.change();
    };
    InterfaceElement.prototype.setReadonly = function (bool) {
        if (this.readonly != bool) {
            this.readonly = bool;
            this.change();
        }
    };
    InterfaceElement.prototype.setValue = function (value) {
        if (this.value != value) {
            if (value == undefined)
                value = '';
            if (this.value == undefined)
                this.value = value;
            else
                this.value = value + (value.length < this.value.length ? Array(this.value.length - value.length + 1).join(' ') : '');
            this.change();
        }
    };
    return InterfaceElement;
}());
exports.__esModule = true;
exports["default"] = InterfaceElement;
//# sourceMappingURL=InterfaceElement.js.map
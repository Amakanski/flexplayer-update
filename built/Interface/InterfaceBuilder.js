"use strict";
var Button_1 = require('./Button');
var Input_1 = require('./Input');
var Label_1 = require('./Label');
var Checkbox_1 = require('./Checkbox');
var Container_1 = require('./Container');
var KeyboardButton_1 = require('./KeyboardButton');
var Vector_1 = require('./Vector');
var InterfaceBuilder = (function () {
    function InterfaceBuilder() {
    }
    InterfaceBuilder.buildLabel = function (json) {
        return new Label_1["default"](json);
    };
    InterfaceBuilder.buildButton = function (json) {
        return new Button_1["default"](json);
    };
    InterfaceBuilder.buildInput = function (json) {
        return new Input_1["default"](json);
    };
    InterfaceBuilder.buildCheckbox = function (json) {
        return new Checkbox_1["default"](json);
    };
    InterfaceBuilder.buildListbox = function (json) {
        return new Container_1["default"](json);
    };
    InterfaceBuilder.buildKeyboardButton = function (json) {
        return new KeyboardButton_1["default"](json);
    };
    InterfaceBuilder.buildKeyboard = function (json) {
        json.container_type = 'keyboard';
        var keyboard = new Container_1["default"](json);
        var margin = new Vector_1["default"](4, 2);
        for (var row = 0; row < json.contents.length; row++) {
            var width = margin.x;
            var indent = json.contents[row].indent;
            for (var column = 0; column < json.contents[row].buttons.length; column++) {
                var keyboardbutton = this.buildKeyboardButton(json.contents[row].buttons[column]);
                keyboardbutton.parent = keyboard;
                keyboardbutton.position.x = width + indent;
                keyboardbutton.position.y = margin.y + 2 * row;
                keyboardbutton.tab.x = column;
                keyboardbutton.tab.y = row;
                keyboard.addChild(keyboardbutton);
                width += keyboardbutton.value[0].length + 5;
                if (width + margin.x > keyboard.size.x)
                    keyboard.size.x = width + margin.x;
            }
            keyboard.size.y = 2 * json.contents.length + margin.y;
        }
        return keyboard;
    };
    InterfaceBuilder.buildContainer = function (json) {
        var container = new Container_1["default"](json);
        var self = this;
        json.contents.forEach(function (child) {
            container.addChild(self.buildAny(child));
        });
        return container;
    };
    InterfaceBuilder.buildAny = function (json) {
        if (json.type == 'title')
            return this.buildLabel(json);
        else if (json.type == 'label')
            return this.buildLabel(json);
        else if (json.type == 'input')
            return this.buildInput(json);
        else if (json.type == 'button')
            return this.buildButton(json);
        else if (json.type == 'checkbox')
            return this.buildCheckbox(json);
        else if (json.type == 'container')
            return this.buildContainer(json);
        else if (json.type == 'keyboard')
            return this.buildKeyboard(json);
        else if (json.type == 'listbox')
            return this.buildListbox(json);
        throw Error('No build strategy for ' + json.type);
    };
    InterfaceBuilder.build = function (json) {
        var self = this;
        var nodes = Array();
        json.forEach(function (node) {
            nodes.push(self.buildAny(node));
        });
        return nodes;
    };
    return InterfaceBuilder;
}());
exports.__esModule = true;
exports["default"] = InterfaceBuilder;
//# sourceMappingURL=InterfaceBuilder.js.map
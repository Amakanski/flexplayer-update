"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MessageEmitter_1 = require('../Message/MessageEmitter');
var InterfaceElement_1 = require('./InterfaceElement');
var KeyboardButton = (function (_super) {
    __extends(KeyboardButton, _super);
    function KeyboardButton(value) {
        _super.call(this, {});
        this.highlight = false;
        this.type = 'KeyboardButton';
        this.id = 'keyboard_' + value;
        this.value = value;
        this.tab = { x: 0, y: 0 };
        var self = this;
        self.event_handler.addListener({
            "on_click": function () {
                self.highlight = true;
                MessageEmitter_1["default"].emit('interface', 'paint', self);
                setTimeout(function () {
                    self.highlight = false;
                    MessageEmitter_1["default"].emit('interface', 'paint', self);
                }, 200);
            }
        });
    }
    return KeyboardButton;
}(InterfaceElement_1["default"]));
exports.__esModule = true;
exports["default"] = KeyboardButton;
//# sourceMappingURL=KeyboardButton.js.map
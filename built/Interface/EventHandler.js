"use strict";
var EventHandler = (function () {
    function EventHandler() {
        this.listeners = new Array();
    }
    EventHandler.prototype.addListener = function (listener) {
        this.listeners.push(listener);
    };
    EventHandler.prototype.popListener = function () {
        return this.listeners.pop();
    };
    EventHandler.prototype.trigger = function (call, value) {
        this.listeners.forEach(function (l) {
            if (typeof l[call] == 'function')
                l[call](value);
        });
    };
    return EventHandler;
}());
exports.__esModule = true;
exports["default"] = EventHandler;
//# sourceMappingURL=EventHandler.js.map
"use strict";
var InterfaceElement_1 = require('./InterfaceElement');
var InterfaceHandler = (function () {
    function InterfaceHandler() {
    }
    InterfaceHandler.bindRootNode = function (node) {
        this.root_node = new InterfaceElement_1["default"]({
            id: 'root',
            display: 'false'
        });
        this.root_node.children = node;
        this.focus = this.root_node.children[0];
    };
    InterfaceHandler.setFocus = function (target) {
        if (this.focus)
            this.focus.event_handler.trigger("on_blur");
        this.focus = target;
        if (this.focus)
            this.focus.event_handler.trigger("on_focus");
    };
    InterfaceHandler.findElementById = function (node, id) {
        if (!node)
            return null;
        if (node.id == id)
            return node;
        for (var i = 0; i < node.children.length; i++) {
            var find = this.findElementById(node.children[i], id);
            if (find)
                return find;
        }
        return null;
    };
    InterfaceHandler.getElementById = function (id) {
        if (this.imap[id])
            return this.imap[id];
        var obj = this.findElementById(this.root_node, id);
        if (!obj)
            throw Error('Element not found(' + id + ')');
        this.imap[id] = obj;
        return obj;
    };
    InterfaceHandler.bubble = function (trigger, data) {
        if (this.focus)
            this.focus.event_handler.trigger(trigger, data);
    };
    InterfaceHandler.bindKeyboard = function (keyboard, event_target, condition) {
        var self = this;
        if (!condition)
            condition = function () { return true; };
        event_target.event_handler.addListener({
            "on_click": function () {
                if (condition()) {
                    self.back_focus = self.focus;
                    self.setFocus(keyboard);
                }
                keyboard.event_handler.addListener({
                    "on_keydown": function (value) {
                        if (condition())
                            event_target.event_handler.trigger("on_keydown", value);
                    }
                });
            },
            "on_enter": function () {
                if (condition())
                    event_target.event_handler.trigger("on_back");
            },
            "on_back": function () {
                if (condition())
                    self.setFocus(self.back_focus);
                keyboard.event_handler.popListener();
            }
        });
    };
    InterfaceHandler.command = function (command) {
        if (!this.interfacing)
            return;
        switch (command) {
            case "up":
                this.bubble("on_up");
                break;
            case "down":
                this.bubble("on_down");
                break;
            case "left":
                this.bubble("on_left");
                break;
            case "right":
                this.bubble("on_right");
                break;
            case "click":
                this.bubble("on_click");
                break;
            case "back":
                this.bubble("on_back");
                break;
        }
    };
    InterfaceHandler.interfacing = false;
    InterfaceHandler.counter = 0;
    InterfaceHandler.back_focus = null;
    InterfaceHandler.focus = null;
    InterfaceHandler.imap = {};
    return InterfaceHandler;
}());
exports.__esModule = true;
exports["default"] = InterfaceHandler;
//# sourceMappingURL=InterfaceHandler.js.map
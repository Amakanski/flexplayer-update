"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InterfaceElement_1 = require('./InterfaceElement');
var Label = (function (_super) {
    __extends(Label, _super);
    function Label(json) {
        _super.call(this, json);
        this.type = 'Label';
        this.display = true;
    }
    return Label;
}(InterfaceElement_1["default"]));
exports.__esModule = true;
exports["default"] = Label;
//# sourceMappingURL=Label.js.map
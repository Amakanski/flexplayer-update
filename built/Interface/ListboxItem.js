"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InterfaceElement_1 = require('./InterfaceElement');
var ListboxItem = (function (_super) {
    __extends(ListboxItem, _super);
    function ListboxItem(json) {
        _super.call(this, json);
        this.selectable = true;
        this.type = 'ListboxItem';
        this.size.x = this.value.length + 1;
    }
    ListboxItem.prototype.setValue = function (value) {
        _super.prototype.setValue.call(this, value);
        this.size.x = value.length + 1;
    };
    return ListboxItem;
}(InterfaceElement_1["default"]));
exports.__esModule = true;
exports["default"] = ListboxItem;
//# sourceMappingURL=ListboxItem.js.map
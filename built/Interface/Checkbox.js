"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InterfaceElement_1 = require('./InterfaceElement');
var Checkbox = (function (_super) {
    __extends(Checkbox, _super);
    function Checkbox(json) {
        var _this = this;
        _super.call(this, json);
        this.checked = false;
        this.type = 'Checkbox';
        this.selectable = true;
        this.checked = false;
        var self = this;
        this.event_handler.addListener({
            "on_click": function () {
                self.checked = !self.checked;
                _this.change();
            }
        });
    }
    Checkbox.prototype.setChecked = function (set) {
        this.checked = set;
        this.change();
    };
    return Checkbox;
}(InterfaceElement_1["default"]));
exports.__esModule = true;
exports["default"] = Checkbox;
//# sourceMappingURL=Checkbox.js.map
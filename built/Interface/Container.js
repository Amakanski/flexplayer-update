"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MessageEmitter_1 = require('../Message/MessageEmitter');
var InterfaceElement_1 = require('./InterfaceElement');
var getContainerListener = function (self) {
    return {
        "on_up": function () {
            if (self.child_focus) {
                self.child_focus.event_handler.trigger('on_blur');
                MessageEmitter_1["default"].emit('paint', self.child_focus);
            }
            do {
                self.focus_index = (self.focus_index > 0) ? self.focus_index - 1 : self.children.length - 1;
            } while (!self.children[self.focus_index].selectable || self.children[self.focus_index].readonly);
            self.child_focus = self.children[self.focus_index];
            self.child_focus.event_handler.trigger('on_focus');
            MessageEmitter_1["default"].emit('paint', self.child_focus);
            self.bubble("on_up");
        },
        "on_down": function () {
            if (self.child_focus) {
                self.child_focus.event_handler.trigger('on_blur');
                MessageEmitter_1["default"].emit('paint', self.child_focus);
            }
            do {
                self.focus_index = (self.focus_index < self.children.length - 1) ? self.focus_index + 1 : 0;
            } while (!self.children[self.focus_index].selectable || self.children[self.focus_index].readonly);
            self.child_focus = self.children[self.focus_index];
            self.child_focus.event_handler.trigger('on_focus');
            MessageEmitter_1["default"].emit('paint', self.child_focus);
            self.bubble("on_down");
        },
        "on_focus": function () { },
        "on_blur": function () { },
        "on_left": function () { self.bubble("on_left"); },
        "on_right": function () { self.bubble("on_right"); },
        "on_click": function () { self.bubble("on_click"); },
        "on_back": function () { self.bubble("on_back"); },
        "on_keydown": function (character) { self.bubble("on_keydown", character); }
    };
};
var findChildByTab = function (children, x, y) {
    var result = children.filter(function (child) { return child.tab.x == x && child.tab.y == y; });
    if (result.length != 1 && x >= 0)
        return findChildByTab(children, x - 1, y);
    return result[0];
};
var initialTabbedChild = function (self) {
    if (!self.child_focus && self.children.length > 0) {
        self.child_focus = self.children[0];
        self.child_focus.event_handler.trigger('on_focus');
        return true;
    }
    return false;
};
var getKeyboardListener = function (self) {
    return {
        "on_focus": function () {
            self.display = true;
            MessageEmitter_1["default"].emit('paint', self);
        },
        "on_blur": function () {
            self.display = false;
            MessageEmitter_1["default"].emit('paint', self);
        },
        "on_up": function () {
            if (!initialTabbedChild(self)) {
                var new_focus = findChildByTab(self.children, self.child_focus.tab.x, self.child_focus.tab.y - 1);
                if (new_focus)
                    self.setFocus(new_focus);
            }
        },
        "on_down": function () {
            if (!initialTabbedChild(self)) {
                var new_focus = findChildByTab(self.children, self.child_focus.tab.x, self.child_focus.tab.y + 1);
                if (new_focus)
                    self.setFocus(new_focus);
            }
        },
        "on_left": function () {
            if (!initialTabbedChild(self)) {
                var new_focus = findChildByTab(self.children, self.child_focus.tab.x - 1, self.child_focus.tab.y);
                if (new_focus)
                    self.setFocus(new_focus);
            }
        },
        "on_right": function () {
            if (!initialTabbedChild(self)) {
                var new_focus = findChildByTab(self.children, self.child_focus.tab.x + 1, self.child_focus.tab.y);
                if (new_focus)
                    self.setFocus(new_focus);
            }
        },
        "on_click": function () {
            self.event_handler.trigger('on_keydown', self.child_focus.value[self.shift]);
            self.bubble("on_click");
        },
        "on_back": function () {
            self.bubble("on_back");
        },
        "on_keydown": function (character) {
            if (character == 'shift') {
                self.shift++;
                if (self.max_shift <= self.shift)
                    self.shift = 0;
                MessageEmitter_1["default"].emit('paint', self);
            }
            else
                self.bubble("on_keydown", character[self.shift]);
        }
    };
};
var getListboxEventHandler = function (self) {
    return {
        "on_up": function () {
            if (self.children.length <= 1)
                return;
            if (self.child_focus) {
                self.child_focus.event_handler.trigger('on_blur');
            }
            do {
                self.focus_index = (self.focus_index > 0) ? self.focus_index - 1 : self.children.length - 1;
            } while (!self.children[self.focus_index].selectable);
            self.child_focus = self.children[self.focus_index];
            self.child_focus.event_handler.trigger('on_focus');
        },
        "on_down": function () {
            if (self.children.length <= 1)
                return;
            if (self.child_focus) {
                self.child_focus.event_handler.trigger('on_blur');
            }
            do {
                self.focus_index = (self.focus_index < self.children.length - 1) ? self.focus_index + 1 : 0;
            } while (!self.children[self.focus_index].selectable);
            self.child_focus = self.children[self.focus_index];
            self.child_focus.event_handler.trigger('on_focus');
        },
        "on_focus": function () {
            self.display = true;
            MessageEmitter_1["default"].emit('paint', self);
        },
        "on_blur": function () {
            self.display = false;
            MessageEmitter_1["default"].emit('paint', self);
        },
        "on_left": function () { },
        "on_right": function () { },
        "on_click": function () {
            self.bubble('on_click');
        },
        "on_back": function () { },
        "on_keydown": function (character) { }
    };
};
var Container = (function (_super) {
    __extends(Container, _super);
    function Container(json) {
        _super.call(this, json);
        this.container_type = '';
        this.child_focus = null;
        this.focus_index = -1;
        this.shift = 0;
        this.max_shift = 2;
        this.header = "";
        var self = this;
        self.type = 'Container';
        self.focus_index = -1;
        self.container_type = json.type || 'container';
        if (json.header)
            self.header = json.header;
        switch (self.container_type) {
            case "container":
                self.event_handler.addListener(getContainerListener(self));
                break;
            case "keyboard":
                this.event_handler.addListener(getKeyboardListener(self));
                break;
            case "listbox":
                this.event_handler.addListener(getListboxEventHandler(self));
                break;
        }
    }
    Container.prototype.bubble = function (trigger, value) {
        this.children.forEach(function (child) {
            if (child.focus)
                child.event_handler.trigger(trigger, value);
        });
    };
    Container.prototype.setFocus = function (new_focus) {
        if (!new_focus)
            return;
        if (this.child_focus)
            this.child_focus.event_handler.trigger('on_blur');
        this.child_focus = new_focus;
        this.child_focus.event_handler.trigger('on_focus');
    };
    return Container;
}(InterfaceElement_1["default"]));
exports.__esModule = true;
exports["default"] = Container;
//# sourceMappingURL=Container.js.map
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InterfaceElement_1 = require('./InterfaceElement');
var Button = (function (_super) {
    __extends(Button, _super);
    function Button(json) {
        _super.call(this, json);
        this.type = 'Button';
        this.selectable = true;
        var self = this;
    }
    return Button;
}(InterfaceElement_1["default"]));
exports.__esModule = true;
exports["default"] = Button;
//# sourceMappingURL=Button.js.map
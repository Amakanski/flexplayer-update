"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MessageEmitter_1 = require('../Message/MessageEmitter');
var InterfaceElement_1 = require('./InterfaceElement');
var Input = (function (_super) {
    __extends(Input, _super);
    function Input(json) {
        _super.call(this, json);
        this.mask = '';
        this.max_length = 99;
        this.type = 'Input';
        this.selectable = true;
        this.mask = json.mask || '';
        this.max_length = json.length || 99;
        this.size.x = 2 + this.max_length;
        var self = this;
        self.event_handler.addListener({
            "on_keydown": function (character) {
                switch (character.trim()) {
                    case 'shift':
                        break;
                    case 'space':
                        if (self.value.length < self.max_length) {
                            self.value += ' ';
                            self.event_handler.trigger('on_change', self.value);
                        }
                        break;
                    case 'backspace':
                        if (self.value.length > 0) {
                            self.value = self.value.substring(0, self.value.length - 1);
                            self.event_handler.trigger('on_change', self.value);
                        }
                        break;
                    case 'enter':
                        self.event_handler.trigger('on_enter', self.value);
                        break;
                    default:
                        if (self.value.length < self.max_length) {
                            self.value += character;
                            self.event_handler.trigger('on_change', self.value);
                        }
                        break;
                }
            },
            "on_change": function () {
                MessageEmitter_1["default"].emit('interface', 'paint', self);
            },
            "on_focus": function () {
                self.focus = true;
                MessageEmitter_1["default"].emit('interface', 'paint', self);
            },
            "on_blur": function () {
                self.focus = false;
                MessageEmitter_1["default"].emit('interface', 'paint', self);
            }
        });
    }
    return Input;
}(InterfaceElement_1["default"]));
exports.__esModule = true;
exports["default"] = Input;
//# sourceMappingURL=Input.js.map
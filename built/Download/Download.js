"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MessageEmitter_1 = require('../Message/MessageEmitter');
var settings = require('../../settings.json');
var Download = (function () {
    function Download(from, to, on_success, on_error) {
        this.downloading = false;
        this.pending = true;
        this.progress = 0;
        this.available = false;
        this.overwrite = false;
        this.error = false;
        this.status = '';
        this.from = from;
        this.to = to;
        this.on_success = on_success;
        this.on_error = on_error;
    }
    return Download;
}());
exports.__esModule = true;
exports["default"] = Download;
var PlaylistDownload = (function (_super) {
    __extends(PlaylistDownload, _super);
    function PlaylistDownload(device) {
        _super.call(this, 'http://' + settings.server + '/data/device/status/' + device.auth_key + '/', settings.cache_folder + settings.device_file, function () {
            MessageEmitter_1["default"].emit('playlist_download', 'on_update');
        }, function () {
        });
        this.name = "Playlist";
    }
    return PlaylistDownload;
}(Download));
exports.PlaylistDownload = PlaylistDownload;
var NodeDownload = (function (_super) {
    __extends(NodeDownload, _super);
    function NodeDownload(device, node, on_success, on_error) {
        var _this = this;
        _super.call(this, 'http://' + settings.server + '/data/device/content/' + device.auth_key + '/' + node.content.url + '/', settings.cache_folder + node.content.url + '.' + node.content.type, function () {
            _this.node.downloading = false;
            _this.node.file_exists = true;
        }, function (error) {
            _this.node.downloading = false;
            _this.error = true;
            _this.status = error;
        });
        this.node = node;
        this.name = node.content.title;
    }
    return NodeDownload;
}(Download));
exports.NodeDownload = NodeDownload;
//# sourceMappingURL=Download.js.map
"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var fs = require('fs');
var http = require('http');
var url = require('url');
var querystring = require('querystring');
var settings = require('../../settings.json');
var WAIT_FOR_NEXT_DOWNLOAD = 0;
var WAIT_WHEN_IDLE = 1000;
var DownloadManager = (function () {
    function DownloadManager() {
    }
    DownloadManager.process_queue = function () {
        if (this.download_queue.length > 0)
            this.download(this.download_queue.shift());
    };
    DownloadManager.post_player_status = function (player) {
        var data = JSON.stringify(player);
        MessageEmitter_1["default"].emit('debug', 'Sending status update');
        var options = {
            host: settings.server,
            port: 80,
            path: '/data/device/status/' + player.device.auth_key + '/',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(data)
            }
        };
        var req = http.request(options, function (res) {
            res.setEncoding('utf8');
        });
        req.on('error', function (e) {
            MessageEmitter_1["default"].emit('debug', 'Error posting status: ' + e.message);
        });
        req.write(data);
        req.end();
    };
    DownloadManager.post = function (post_data) {
        var data = querystring.stringify(post_data);
        var options = {
            host: settings.server,
            port: 80,
            path: '/data/devices/',
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(data)
            }
        };
        var req = http.request(options, function (res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) { });
        });
        req.write(data);
        req.end();
    };
    DownloadManager.download = function (download) {
        if (this.download_queue.some(function (d) { return d.from == download.from; })) {
            MessageEmitter_1["default"].emit("debug", "Already downloading: " + download.from);
            return;
        }
        if (this.downloading) {
            this.download_queue.push(download);
            return;
        }
        this.downloading = download;
        download.downloading = true;
        var temp_filename = settings.cache_folder + 'downloading.tmp';
        var file = fs.createWriteStream(temp_filename);
        var from_url = url.parse(download.from);
        var request = http.get({ host: from_url.host, port: '80', path: from_url.path }, function (response) {
            response.on('data', function (data) {
                download.progress += data.length;
                if (download.size > 0)
                    download.status = Math.round(100 * download.progress / download.size) + '%';
                else
                    download.status = 'Downloading';
                MessageEmitter_1["default"].emit('download', 'on_progress', download);
            });
            var location = response.headers.location;
            if (location) {
                DownloadManager.downloading = null;
                download.from = location;
                MessageEmitter_1["default"].emit('download', 'on_redirect', download);
                DownloadManager.download(download);
            }
            else {
                if (response.statusCode == 200) {
                    response.pipe(file);
                    if (fs.existsSync(download.to))
                        fs.unlinkSync(download.to);
                    fs.renameSync(temp_filename, download.to);
                    MessageEmitter_1["default"].emit('download', 'on_complete');
                    file.on('finish', function () {
                        DownloadManager.downloading = null;
                        download.available = true;
                        download.downloading = false;
                        file.close();
                        download.on_success();
                        DownloadManager.process_queue();
                    });
                }
                else {
                    MessageEmitter_1["default"].emit('debug', 'Download error: ' + download.from);
                    MessageEmitter_1["default"].emit('debug', 'Error: ' + response.statusCode);
                    if (fs.existsSync(temp_filename))
                        fs.unlinkSync(temp_filename);
                    DownloadManager.downloading = null;
                    download.downloading = false;
                    download.error = true;
                    download.status = 'Error ' + response.statusCode;
                    download.on_error(response.statusCode);
                    DownloadManager.process_queue();
                }
            }
        })
            .on('response', function (data) {
            if (data.headers['content-length']) {
                download.size = data.headers['content-length'];
            }
        })
            .on('error', function (err) {
            MessageEmitter_1["default"].emit('debug', 'Download error: ' + download.from);
            MessageEmitter_1["default"].emit('debug', 'Error: ' + err);
            download.downloading = false;
            download.error = true;
            fs.unlink(download.to);
            download.on_error(err);
            DownloadManager.downloading = null;
            DownloadManager.process_queue();
        });
    };
    ;
    DownloadManager.download_queue = Array();
    DownloadManager.downloading = null;
    DownloadManager.paused = false;
    return DownloadManager;
}());
exports.__esModule = true;
exports["default"] = DownloadManager;
//# sourceMappingURL=DownloadManager.js.map
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Download_1 = require('../Download/Download');
var Playlist = (function (_super) {
    __extends(Playlist, _super);
    function Playlist() {
        _super.call(this);
        this.title = '';
        this.duration = 0;
        this.file_size = 0;
        this.items = new Array();
    }
    return Playlist;
}(Download_1["default"]));
exports.__esModule = true;
exports["default"] = Playlist;
//# sourceMappingURL=__Playlist.js.map
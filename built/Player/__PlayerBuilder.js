"use strict";
var Player_1 = require('./Player');
var Playlist_1 = require('./Playlist');
var PlaylistItem_1 = require('./PlaylistItem');
var MessageEmitter_1 = require('../Message/MessageEmitter');
var PlayerBuilder;
(function (PlayerBuilder) {
    function buildPlaylistItem(json) {
        MessageEmitter_1["default"].emit('debug', 'PlayerBuilder::buildPlaylistItem()');
        var item = new PlaylistItem_1["default"]();
        item.content_id = json.content_id ? json.content_id : -1;
        item.type = json.type ? json.type : "Unknown";
        item.title = json.title ? json.title : "Unknown";
        item.name = item.title;
        item.duration = json.duration ? json.duration : -1;
        item.file_size = json.file_size ? json.file_size : -1;
        item.external_storage = json.external_storage ? json.external_storage : "None";
        item.url_remote = item.external_storage;
        item.file_extention = item.external_storage.indexOf('.') != -1 ? item.external_storage.split('.').pop() : "";
        item.folder_local = 'cache/';
        item.filename = item.content_id != -1 && item.file_extention ? item.content_id + '.' + item.file_extention : '';
        item.available = false;
        item.downloading = false;
        item.download_progress = 0;
        return item;
    }
    PlayerBuilder.buildPlaylistItem = buildPlaylistItem;
    function buildPlaylistItems(json) {
        MessageEmitter_1["default"].emit('debug', 'PlayerBuilder::buildPlaylistItems()');
        var items = Array();
        json.forEach(function (item) {
            items.push(PlayerBuilder.buildPlaylistItem(item));
        });
        return items;
    }
    PlayerBuilder.buildPlaylistItems = buildPlaylistItems;
    function buildPlaylist(json) {
        MessageEmitter_1["default"].emit('debug', 'PlayerBuilder::buildPlaylist()');
        var playlist = new Playlist_1["default"]();
        playlist.title = json.title ? json.title : 'Unknown playlist';
        playlist.duration = 0;
        playlist.file_size = 0;
        if (json.items)
            playlist.items = PlayerBuilder.buildPlaylistItems(json.items);
        return playlist;
    }
    PlayerBuilder.buildPlaylist = buildPlaylist;
    function buildPlayer(json) {
        MessageEmitter_1["default"].emit('debug', 'PlayerBuilder::buildPlayer()');
        var player = new Player_1["default"]();
        player.type = json.type ? json.type : 'Unknown';
        if (json.playlist)
            player.playlist = PlayerBuilder.buildPlaylist(json.playlist);
        return player;
    }
    PlayerBuilder.buildPlayer = buildPlayer;
    function build(json) {
        MessageEmitter_1["default"].emit('debug', 'PlayerBuilder::build()');
        return PlayerBuilder.buildPlayer(json);
    }
    PlayerBuilder.build = build;
})(PlayerBuilder = exports.PlayerBuilder || (exports.PlayerBuilder = {}));
//# sourceMappingURL=__PlayerBuilder.js.map
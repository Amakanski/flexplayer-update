"use strict";
var Language = (function () {
    function Language(data) {
        if (data != null) {
            this.id = data.id;
            this.short = data.short;
            this.long = data.long;
            this.available = data.available == '1';
        }
    }
    return Language;
}());
exports.Language = Language;
var Branche = (function () {
    function Branche(data) {
        this.id = data.id;
        this.name = data.name;
    }
    return Branche;
}());
exports.Branche = Branche;
var Brand = (function () {
    function Brand(data) {
        this.id = data.id;
        this.name = data.name;
        this.url = data.url;
        this.description = data.description;
    }
    return Brand;
}());
exports.Brand = Brand;
var PlaylistNode = (function () {
    function PlaylistNode(data) {
        this.id = data.id;
        this.order = data.order;
        this.content = data.content ? new Content(data.content) : null;
        this.play_from = data.play_from ? new Date(data.play_from.date) : null;
        this.play_until = data.play_until ? new Date(data.play_until.date) : null;
        this.deleted = data.deleted == '1';
        this.created_on = data.created_on ? new Date(data.created_on.date) : null;
        this.file_exists = false;
        this.is_playing = false;
        this.downloading = false;
    }
    PlaylistNode.prototype.is_available = function () {
        if (this.play_from && this.play_from.getTime() > new Date().getTime())
            return false;
        if (this.play_until && this.play_until.getTime() < new Date().getTime())
            return false;
        if (this.content && this.content.available_from && this.content.available_from.getTime() > new Date().getTime())
            return false;
        if (this.content && this.content.available_until && this.content.available_until.getTime() < new Date().getTime())
            return false;
        if (!this.file_exists)
            return false;
        return true;
    };
    return PlaylistNode;
}());
exports.PlaylistNode = PlaylistNode;
var Playlist = (function () {
    function Playlist(data) {
        var _this = this;
        this.nodes = new Array();
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.url = data.url;
            this.owner = data.owner ? new Account(data.owner) : null;
            this.duration = data.duration;
            this.file_size = data.file_size;
            this.deleted = data.deleted == '1';
            this.last_modified = data.last_modified ? new Date(data.last_modified.date) : null;
            data.nodes.forEach(function (node) {
                _this.nodes.push(new PlaylistNode(node));
            });
        }
    }
    return Playlist;
}());
exports.Playlist = Playlist;
var Content = (function () {
    function Content(data) {
        this.id = data.id;
        this.title = data.title;
        this.url = data.url;
        this.owner = data.owner ? new Account(data.owner) : null;
        this.brand = data.brand ? new Brand(data.brand) : null;
        this.branche = data.branche ? new Branche(data.branche) : null;
        this.description = data.description;
        this.language = data.language ? new Language(data.language) : null;
        this.type = data.type;
        this.playlist = data.playlist ? new Playlist(data.playlist) : null;
        this.duration = data.duration;
        this.width = data.width;
        this.height = data.height;
        this.views = data.views;
        this.replacement = data.replacement ? new Content(data.replacement) : null;
        this.available_from = data.available_from ? new Date(data.available_from.date) : null;
        this.available_until = data.available_until ? new Date(data.available_until.date) : null;
        this.created_on = data.created_on ? new Date(data.created_on.date) : null;
    }
    return Content;
}());
exports.Content = Content;
var Account = (function () {
    function Account(data) {
        this.id = data.id;
        this.name = data.name;
    }
    return Account;
}());
exports.Account = Account;
var DeviceInfo = (function () {
    function DeviceInfo(data) {
        this.id = data.id;
        this.name = data.name;
        this.manufacturer = data.manufacturer;
        this.image = data.image;
        this.available_from = data.available_from ? new Date(data.available_from.date) : null;
        this.conversion_rule = data.conversion_rule ? new ConversionRule(data.conversion_rule) : null;
    }
    return DeviceInfo;
}());
exports.DeviceInfo = DeviceInfo;
var ConversionRule = (function () {
    function ConversionRule(data) {
        this.id = data.id;
        this.description = data.description;
        this.type = data.type;
        this.scaling = data.scaling;
        this.frame_dx = data.frame_dx;
        this.frame_dy = data.frame_dy;
        this.bitrate_video = data.bitrate_video;
        this.bitrate_audio = data.bitrate_audio;
        this.orientation = data.orientation;
    }
    return ConversionRule;
}());
exports.ConversionRule = ConversionRule;
var Device = (function () {
    function Device(data) {
        this.id = data.id;
        this.device_info = data.device_info ? new DeviceInfo(data.device_info) : null;
        this.mac = data.mac;
        this.auth_key = data.auth_key;
        this.description = data.description;
        this.owner = data.owner ? new Account(data.owner) : null;
        this.ip_remote = data.ip_remote;
        this.ip_eth0 = data.ip_eth0;
        this.ip_wlan0 = data.ip_wlan0;
        this.last_contact = data.last_contact ? new Date(data.last_contact.date) : null;
        this.storage = data.storage;
        this.storage_free = data.storage_free;
        this.playing = data.playing ? new Content(data.playing) : null;
        this.progress = data.progress;
        this.playlist = new Playlist(data.playlist);
        this.conversion_rule = data.conversion_rule ? new ConversionRule(data.conversion_rule) : null;
    }
    return Device;
}());
exports.__esModule = true;
exports["default"] = Device;
//# sourceMappingURL=Device.js.map
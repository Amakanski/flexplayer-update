"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var MessageEmitter_1 = require('../Message/MessageEmitter');
var PlayerManager = (function (_super) {
    __extends(PlayerManager, _super);
    function PlayerManager() {
        _super.call(this);
    }
    PlayerManager.setPlayer = function (player) {
        this.player = player;
        this.emit("new_player", player);
    };
    PlayerManager.updatePlaylist = function (playlist) {
        this.player.playlist = playlist;
        this.emit("update_playlist", playlist);
    };
    PlayerManager.play = function (playlistitem) {
        this.playing = playlistitem;
        this.emit("play", playlistitem);
    };
    PlayerManager.start = function () {
        if (this.player.playlist.items.length == 0)
            return this.emit("empty_playlist");
        if (!this.playing)
            this.play(this.player.playlist.items[0]);
    };
    return PlayerManager;
}(MessageEmitter_1["default"]));
exports.__esModule = true;
exports["default"] = PlayerManager;
//# sourceMappingURL=__PlayerManager.js.map
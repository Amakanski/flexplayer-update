"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var Download_1 = require('../Download/Download');
var PlaylistItem = (function (_super) {
    __extends(PlaylistItem, _super);
    function PlaylistItem() {
        _super.call(this);
    }
    return PlaylistItem;
}(Download_1["default"]));
exports.__esModule = true;
exports["default"] = PlaylistItem;
//# sourceMappingURL=PlaylistItem.js.map
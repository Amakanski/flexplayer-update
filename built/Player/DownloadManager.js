"use strict";
var PlaylistItemObserver = (function () {
    function PlaylistItemObserver(item) {
        this.item = item;
    }
    PlaylistItemObserver.prototype.on = function (message, data) {
    };
    return PlaylistItemObserver;
}());
var DownloadManager = (function () {
    function DownloadManager() {
        this.storage = 'data/';
    }
    DownloadManager.prototype.preparePlaylistItem = function (item) {
    };
    DownloadManager.prototype.on = function (event, data) {
        switch (event) {
            case "on_new_playlistitem":
                if (typeof data != 'PlaylistItem')
                    throw new Error('on_new_playlistitem error, data != PlaylistItem');
                else
                    return new PlaylistItemObserver(data);
        }
    };
    return DownloadManager;
}());
exports.__esModule = true;
exports["default"] = DownloadManager;
//# sourceMappingURL=DownloadManager.js.map
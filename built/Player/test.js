"use strict";
var PlayerBuilder_1 = require('./PlayerBuilder');
var player_json = {
    "ip": "192.168.1.100",
    "mac": "01:23:45:67:89",
    "playlist": {
        "title": "Een playlist",
        "duration": 0,
        "file_size": 0,
        "items": [
            {
                "content_id": 1,
                "type": "movie",
                "title": "Een testfilm",
                "duration": 120,
                "file_size": 123456,
                "external_storage": "http://www.optiektv.nl/winkel/content/1.mp4"
            },
            {
                "content_id": 2,
                "type": "movie",
                "title": "Andere film",
                "duration": 240,
                "file_size": 567890,
                "external_storage": "http://www.optiektv.nl/winkel/content/2.mp4"
            },
            {
                "content_id": 3,
                "type": "slide",
                "title": "Afbeelding und so",
                "duration": 20,
                "file_size": 1091923,
                "external_storage": "http://www.optiektv.nl/winkel/content/3.png"
            },
        ]
    }
};
var player = PlayerBuilder_1.PlayerBuilder.build(player_json);
console.log('dit is een test');
//# sourceMappingURL=test.js.map
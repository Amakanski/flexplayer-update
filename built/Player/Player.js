"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var DownloadManager_1 = require('../Download/DownloadManager');
var ChildProcess = require('child_process');
var Player = (function () {
    function Player(device) {
        this.play_index = 0;
        this.allowed_content_types = [
            "movie",
            "image",
            "slide"
        ];
        this.device = device;
    }
    Player.prototype.stop = function () {
        if (this.process && this.playing) {
            MessageEmitter_1["default"].emit('debug', 'Disconnecting process #' + this.process.pid);
            ChildProcess.spawn('sh', ['./stop.sh']);
            this.process.kill('SIGKILL');
            this.process = null;
        }
    };
    Player.prototype.play = function () {
        if (this.playing)
            return;
        if (!this.device || !this.device.playlist)
            return;
        var that = this;
        var playing = this.device.playlist.nodes[this.play_index];
        if (playing && playing.is_available()) {
            this.playing = playing;
            this.device.playing = playing.content;
            MessageEmitter_1["default"].emit('debug', 'Playing: ' + this.playing.content.title);
            playing.is_playing = true;
            if (this.allowed_content_types.indexOf(playing.content.type) == -1)
                return MessageEmitter_1["default"].emit('player', 'on_error', 'Unknown content type');
            DownloadManager_1["default"].post_player_status(this);
            var commands = ['./play_' + playing.content.type + '.sh', playing.content.url + '.' + playing.content.type, playing.content.duration];
            var that = this;
            this.process = ChildProcess.spawn('sh', commands);
            this.process.on('close', function (code) {
                if (that.playing)
                    that.playing.is_playing = false;
                that.playing = null;
                MessageEmitter_1["default"].emit('player', 'on_finish', code);
            });
        }
        else
            MessageEmitter_1["default"].emit('player', 'on_error', 'No available node at index');
    };
    Player.prototype.previous = function () {
        this.stop();
        if (this.device.playlist.nodes.length != 0) {
            this.play_index--;
            if (this.play_index < 0)
                this.play_index = this.device.playlist.nodes.length;
            this.play();
        }
    };
    Player.prototype.next = function () {
        this.stop();
        if (this.device.playlist.nodes.length != 0) {
            this.play_index++;
            if (this.play_index >= this.device.playlist.nodes.length)
                this.play_index = 0;
            this.play();
        }
    };
    Player.prototype.rewind = function () {
        this.stop();
        this.play_index = 0;
        this.play();
    };
    return Player;
}());
exports.__esModule = true;
exports["default"] = Player;
//# sourceMappingURL=Player.js.map
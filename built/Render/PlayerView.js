"use strict";
var Draw_1 = require('../Render/Draw');
var PlayerView = (function () {
    function PlayerView() {
    }
    PlayerView.render = function (box, player) {
        Draw_1["default"].setColor(7, 0, true);
        Draw_1["default"].paintBox([box[0] - 1, box[1] - 1, box[2] + 1, box[3] + 1]);
        function sec(i) {
            var secs = Math.floor(0.001 * i);
            var minutes = Math.floor(secs / 60);
            var hours = Math.floor(secs / (60 * 60));
            var days = Math.floor(secs / (60 * 60 * 24));
            var seconds = secs % 60;
            if (Math.abs(days) > 0)
                return days + ' d';
            if (Math.abs(hours) > 0)
                return hours + ' h';
            if (Math.abs(minutes) > 0)
                return minutes + ' m';
            return seconds + ' s';
        }
        var y = 0;
        if (player.device && player.device.playlist && player.device.playlist.nodes)
            player.device.playlist.nodes.forEach(function (node) {
                Draw_1["default"].setCursor(box[0], box[1] + y);
                if (!node.is_available())
                    Draw_1["default"].setColor(7, 0, false);
                else
                    Draw_1["default"].setColor(7, 0, true);
                Draw_1["default"].paintText((node.is_playing ? '> ' : ' ') + node.content.title);
                Draw_1["default"].setCursor(box[0] + 20, box[1] + y);
                Draw_1["default"].paintText(node.file_exists ? 'fe' : '');
                Draw_1["default"].setCursor(box[0] + 24, box[1] + y);
                Draw_1["default"].paintText(node.downloading ? 'dl' : '');
                if (node.play_from) {
                    Draw_1["default"].setCursor(box[0] + 30, box[1] + y);
                    Draw_1["default"].paintText(sec(node.play_from.getTime() - new Date().getTime()));
                }
                if (node.play_until) {
                    Draw_1["default"].setCursor(box[0] + 40, box[1] + y);
                    Draw_1["default"].paintText(sec(node.play_until.getTime() - new Date().getTime()));
                }
                if (node.content.available_from) {
                    Draw_1["default"].setCursor(box[0] + 50, box[1] + y);
                    Draw_1["default"].paintText(sec(node.content.available_from.getTime() - new Date().getTime()));
                }
                if (node.content.available_until) {
                    Draw_1["default"].setCursor(box[0] + 60, box[1] + y);
                    Draw_1["default"].paintText(sec(node.content.available_until.getTime() - new Date().getTime()));
                }
                y++;
            });
    };
    return PlayerView;
}());
exports.__esModule = true;
exports["default"] = PlayerView;
//# sourceMappingURL=PlayerView.js.map
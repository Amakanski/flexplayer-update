"use strict";
var Draw_1 = require('./Draw');
var DownloadManager_1 = require('../Download/DownloadManager');
var DownloadView = (function () {
    function DownloadView() {
    }
    DownloadView.render = function (box) {
        Draw_1["default"].setColor(7, 0, true);
        Draw_1["default"].paintBox([box[0] - 1, box[1] - 1, box[2] + 1, box[3] - 1]);
        if (DownloadManager_1["default"].downloading) {
            Draw_1["default"].setCursor(box[0], box[1]);
            Draw_1["default"].paintText(DownloadManager_1["default"].downloading.from);
            Draw_1["default"].setCursor(box[0] + box[2] - DownloadManager_1["default"].downloading.status.length - 2, box[1]);
            Draw_1["default"].paintText(DownloadManager_1["default"].downloading.status);
        }
        var y = box[1] + 1;
        DownloadManager_1["default"].download_queue
            .forEach(function (download) {
            if (y > box[1] + box[3])
                return;
            Draw_1["default"].setColor(7, 0, false);
            Draw_1["default"].setCursor(box[0], y);
            Draw_1["default"].paintText(download.name);
            var str_status = download.status;
            var align_right = str_status.length;
            if (download.downloading)
                Draw_1["default"].setColor(7, 0, true);
            else if (download.available)
                Draw_1["default"].setColor(3, 0, false);
            else if (download.error)
                Draw_1["default"].setColor(2, 0, true);
            else
                Draw_1["default"].setColor(7, 0, true);
            Draw_1["default"].setCursor(box[0] + box[2] - align_right, y);
            Draw_1["default"].paintText(str_status, box[2]);
            y++;
        });
    };
    return DownloadView;
}());
exports.__esModule = true;
exports["default"] = DownloadView;
//# sourceMappingURL=DownloadView.js.map
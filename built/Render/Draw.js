"use strict";
var Draw = (function () {
    function Draw() {
    }
    Draw.clear = function () {
        process.stdout.write(this.ansi + "[2J" + this.ansi + "[;H");
    };
    Draw.showCursor = function (bool) {
        if (bool)
            process.stdout.write(this.ansi + "[?25h");
        else
            process.stdout.write(this.ansi + "[?25l");
    };
    Draw.setCursor = function (x, y) {
        process.stdout.write(this.ansi + "[" + (y + 1) + ";" + (x + 1) + "H");
    };
    Draw.setColor = function (f, b, intense) {
        if (intense)
            process.stdout.write(this.ansi + "[1m");
        else
            process.stdout.write(this.ansi + "[2m");
        process.stdout.write(this.ansi + "[" + (30 + f) + ";" + (40 + b) + "m");
    };
    Draw.setRGB = function (foreground, background) {
        if (foreground)
            process.stdout.write(this.ansi + "[38;2;" + foreground[0] + ";" + foreground[1] + ";" + foreground[2] + "m");
        if (background)
            process.stdout.write(this.ansi + "[48;2;" + background[0] + ";" + background[1] + ";" + background[2] + "m");
    };
    Draw.replaceAt = function (source, index, target) {
        return source.substr(0, index) + target + source.substr(index + target.length);
    };
    Draw.clearBox = function (box) {
        var left = box[0];
        var top = box[1];
        var width = box[2];
        var height = box[3];
        var str = new Array(box[2]).join(' ');
        for (var y = 0; y <= height; y++) {
            Draw.setCursor(left, top + y);
            Draw.paintText(str);
        }
    };
    Draw.paintBox = function (box) {
        var left = box[0];
        var top = box[1];
        var width = box[2];
        var height = box[3];
        if (width < 2)
            width = 2;
        this.setCursor(left, top);
        this.paintText(Draw.BOX.lefttop + new Array(width - 2).join(Draw.BOX.top) + Draw.BOX.righttop);
        var line = Draw.BOX.left + new Array(width - 2).join(' ') + Draw.BOX.right;
        for (var y = 1; y < height; y++) {
            this.setCursor(left, top + y);
            this.paintText(line);
        }
        this.setCursor(left, top + height);
        this.paintText(Draw.BOX.leftbottom + new Array(width - 2).join(Draw.BOX.bottom) + Draw.BOX.rightbottom);
    };
    Draw.paintText = function (str, length) {
        if (length && str.length > length)
            str = str.substr(0, length);
        process.stdout.write(str);
    };
    Draw.use_rgb = false;
    Draw.ansi = "\u001b";
    Draw.colors = {
        "black": 0,
        "red": 1,
        "green": 2,
        "yellow": 3,
        "blue": 4,
        "magenta": 5,
        "cyan": 6,
        "white": 7
    };
    Draw.BOX = {
        "lefttop": String.fromCharCode(0x250c),
        "top": String.fromCharCode(0x2500),
        "righttop": String.fromCharCode(0x2510),
        "left": String.fromCharCode(0x2502),
        "right": String.fromCharCode(0x2502),
        "leftbottom": String.fromCharCode(0x2514),
        "bottom": String.fromCharCode(0x2500),
        "rightbottom": String.fromCharCode(0x2518)
    };
    Draw.padString = function (str, length, mask) {
        if (str == undefined)
            str = 'Undefined';
        var result = str.substring(0, length);
        while (result.length < length)
            result += " ";
        if (mask && mask != '')
            for (var i = 0; i < result.length; i++)
                if (result.substr(i, 1) != ' ')
                    result = result.substr(0, i) + mask.substr(i % mask.length, 1) + result.substr(i + 1);
        return result;
    };
    return Draw;
}());
exports.__esModule = true;
exports["default"] = Draw;
//# sourceMappingURL=Draw.js.map
"use strict";
var Draw_1 = require('./Draw');
var InterfaceHandler_1 = require('../Interface/InterfaceHandler');
var InterfaceView = (function () {
    function InterfaceView() {
    }
    InterfaceView.getPosition = function (element) {
        if (element.parent)
            return element.position.add(this.getPosition(element.parent));
        return element.position;
    };
    InterfaceView.renderContainer = function (container) {
        var position = this.getPosition(container);
        if (container.container_type == 'container') {
            Draw_1["default"].setColor(7, 0, true);
            Draw_1["default"].clearBox([position.x, position.y, container.size.x, container.size.y]);
        }
        else if (container.container_type == 'listbox') {
            var width = 0;
            container.children
                .forEach(function (child) {
                if (child.size.x > width)
                    width = child.size.x;
            });
            container.children
                .forEach(function (child) {
                child.size.x = width;
            });
            Draw_1["default"].setColor(7, 4, true);
            Draw_1["default"].paintBox([position.x, position.y, width + 2, container.children.length + 1]);
        }
        else if (container.container_type == 'keyboard') {
            Draw_1["default"].setColor(7, 0, false);
            Draw_1["default"].clearBox([position.x, position.y, container.size.x, container.size.y]);
        }
    };
    InterfaceView.renderLabel = function (label) {
        var position = this.getPosition(label);
        Draw_1["default"].setCursor(position.x, position.y);
        Draw_1["default"].setColor(7, 0, true);
        Draw_1["default"].paintText(label.value);
    };
    InterfaceView.renderButton = function (button) {
        if (button.focus)
            Draw_1["default"].setColor(7, 1, true);
        else if (button.readonly)
            Draw_1["default"].setColor(7, 0, false);
        else
            Draw_1["default"].setColor(7, 4, false);
        var position = this.getPosition(button);
        Draw_1["default"].paintBox([position.x, position.y, button.value.length + 5, 2]);
        Draw_1["default"].setCursor(position.x + 1, position.y + 1);
        Draw_1["default"].paintText(' ' + button.value + ' ');
    };
    InterfaceView.renderCheckbox = function (checkbox) {
        if (checkbox.focus)
            Draw_1["default"].setColor(7, 1, true);
        else
            Draw_1["default"].setColor(7, 4, true);
        var position = this.getPosition(checkbox);
        Draw_1["default"].setCursor(position.x, position.y);
        if (checkbox.checked)
            Draw_1["default"].paintText(' ' + String.fromCharCode(0x2713) + ' ');
        else
            Draw_1["default"].paintText('   ');
    };
    InterfaceView.renderInput = function (input) {
        if (input.focus)
            Draw_1["default"].setColor(7, 1, true);
        else if (input.readonly)
            Draw_1["default"].setColor(7, 0, false);
        else
            Draw_1["default"].setColor(7, 4, true);
        var position = this.getPosition(input);
        Draw_1["default"].setCursor(position.x, position.y);
        Draw_1["default"].paintText(' ' + Draw_1["default"].padString(input.value, input.size.x, input.mask) + ' ');
    };
    InterfaceView.renderKeyboardButton = function (button) {
        if (button.highlight)
            Draw_1["default"].setColor(7, 2, true);
        else if (button.focus)
            Draw_1["default"].setColor(7, 1, true);
        else
            Draw_1["default"].setColor(7, 4, true);
        var position = this.getPosition(button);
        Draw_1["default"].setCursor(position.x, position.y);
        Draw_1["default"].paintText('  ' + button.value[button.parent.shift] + '  ');
    };
    InterfaceView.renderListboxItem = function (item) {
        var position = this.getPosition(item);
        if (item.focus)
            Draw_1["default"].setColor(7, 1, true);
        else
            Draw_1["default"].setColor(7, 4, true);
        Draw_1["default"].setCursor(position.x + 1, position.y + item.parent_index);
        Draw_1["default"].paintText(Draw_1["default"].padString(item.value, item.size.x - 1));
    };
    InterfaceView.render = function (element) {
        if (!element.display || !InterfaceHandler_1["default"].interfacing)
            return;
        if (element.type == 'Button')
            InterfaceView.renderButton(element);
        else if (element.type == 'Input')
            InterfaceView.renderInput(element);
        else if (element.type == 'KeyboardButton')
            InterfaceView.renderKeyboardButton(element);
        else if (element.type == 'Container')
            InterfaceView.renderContainer(element);
        else if (element.type == 'Label')
            InterfaceView.renderLabel(element);
        else if (element.type == 'Checkbox')
            InterfaceView.renderCheckbox(element);
        else if (element.type == 'ListboxItem')
            InterfaceView.renderListboxItem(element);
        var self = this;
        element.children.forEach(function (child) {
            self.render(child);
        });
    };
    return InterfaceView;
}());
exports.__esModule = true;
exports["default"] = InterfaceView;
//# sourceMappingURL=InterfaceView.js.map
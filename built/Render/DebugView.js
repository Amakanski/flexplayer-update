"use strict";
var Draw_1 = require('./Draw');
var VerboseLogic_1 = require('../Main/VerboseLogic');
var DebugView = (function () {
    function DebugView() {
    }
    DebugView.render = function (box) {
        Draw_1["default"].setColor(7, 0, true);
        Draw_1["default"].paintBox([box[0] - 1, box[1] - 1, box[2] + 1, box[3] + 1]);
        var y = box[3] + box[1] - 1;
        VerboseLogic_1["default"].lines.slice().reverse()
            .forEach(function (line) {
            if (y < box[1])
                return;
            Draw_1["default"].setColor(7, 0, false);
            Draw_1["default"].setCursor(box[0], y);
            Draw_1["default"].paintText(line);
            y--;
        });
    };
    return DebugView;
}());
exports.__esModule = true;
exports["default"] = DebugView;
//# sourceMappingURL=DebugView.js.map
"use strict";
var MessageEmitter_1 = require('./Message/MessageEmitter');
var DownloadManager_1 = require('./Download/DownloadManager');
var Download_1 = require('./Download/Download');
var PlayerBuilder_1 = require('./Player/PlayerBuilder');
var Draw_1 = require('./Render/Draw');
var DownloadView_1 = require('./Render/DownloadView');
var PlayerView_1 = require('./Render/PlayerView');
var settings = require('../settings.json');
var ProcessManager_1 = require('./Process/ProcessManager');
var Processx_1 = require('./Process/Processx');
var test_mac_ip = "eth0      Link encap:Ethernet  HWaddr b8:27:eb:a0:e0:d3\n" +
    "          inet6 addr: fe80::7def:76b9:1452:1554/64 Scope:Link\n" +
    "          UP BROADCAST MULTICAST  MTU:1500  Metric:1\n" +
    "          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\n" +
    "          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\n" +
    "          collisions:0 txqueuelen:1000\n" +
    "          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)\n" +
    "\n" +
    "lo        Link encap:Local Loopback\n" +
    "          inet addr:127.0.0.1  Mask:255.0.0.0\n" +
    "          inet6 addr: ::1/128 Scope:Host\n" +
    "          UP LOOPBACK RUNNING  MTU:65536  Metric:1\n" +
    "          RX packets:0 errors:0 dropped:0 overruns:0 frame:0\n" +
    "          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0\n" +
    "          collisions:0 txqueuelen:0\n" +
    "          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)\n" +
    "\n" +
    "wlan0     Link encap:Ethernet  HWaddr 00:0f:55:b1:1e:bd\n" +
    "          inet addr:192.168.1.15  Bcast:192.168.1.255  Mask:255.255.255.0\n" +
    "          inet6 addr: fe80::c72c:3794:c266:f2b6/64 Scope:Link\n" +
    "          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1\n" +
    "          RX packets:64465174 errors:0 dropped:0 overruns:0 frame:0\n" +
    "          TX packets:12066 errors:0 dropped:0 overruns:0 carrier:0\n" +
    "          collisions:0 txqueuelen:1000\n" +
    "          RX bytes:4180733122 (3.8 GiB)  TX bytes:880893 (860.2 KiB)\n" +
    "\n";
var test_cpuid = "Serial          : 0000000070a0e0d3";
var player_json = {
    "lan": {
        "device": "eth0",
        "mac": "01:23:45:67:89",
        "ip": "192.168.1.100"
    },
    "wireless": {
        "device": "wlan0",
        "mac": "01:23:45:67:89",
        "ip": "192.168.1.100"
    },
    "playlist": {
        "title": "Een playlist",
        "duration": 0,
        "file_size": 0,
        "items": [
            {
                "content_id": 1,
                "type": "movie",
                "title": "Een testfilm",
                "duration": 120,
                "file_size": 123456,
                "external_storage": "http://www.optiektv.nl/winkel/content/1.mp4"
            },
            {
                "content_id": 2,
                "type": "movie",
                "title": "Andere film",
                "duration": 240,
                "file_size": 567890,
                "external_storage": "http://www.optiektv.nl/winkel/content/2.mp4"
            },
            {
                "content_id": 3,
                "type": "slide",
                "title": "Afbeelding und so",
                "duration": 20,
                "file_size": 1091923,
                "external_storage": "http://www.optiektv.nl/winkel/content/3.png"
            },
        ]
    }
};
var start_time = new Date().getTime();
var output = function (str) {
};
var net_process = new Processx_1["default"]('ifconfig eth0');
var PlayerLogic = (function () {
    function PlayerLogic(player) {
        this.downloadview_box = [0, 1, 70, 10];
        this.playerview_box = [72, 1, 70, 10];
        this.player = player;
    }
    PlayerLogic.prototype.paint = function () {
        DownloadView_1["default"].render(this.downloadview_box);
        PlayerView_1["default"].render(this.playerview_box, this.player);
    };
    PlayerLogic.prototype.on = function (message, data) {
        var self = this;
        self.paint();
        switch (message) {
            case "debug":
                break;
            case "player::error":
                break;
            case "process::start":
                return output("starting: " + data.cmd);
            case "process::stop":
                return output("stopping: " + data.cmd);
            case "process::stdout":
                if (data.process == net_process) {
                    var str_output = test_mac_ip;
                    var regex_eth0_hw = /eth0.+HWaddr ([0-9a-f:]+)/;
                    var regex_eth0_ip = /eth0[\s\S]+inet addr:([0-9\.]+)/;
                    var regex_wlan0_hw = /wlan0.+HWaddr ([0-9a-f:]+)/;
                    var regex_wlan0_ip = /wlan0[\s\S]+inet addr:([0-9\.]+)/;
                    var match = regex_eth0_hw.exec(str_output);
                    if (match)
                        player.eth0_mac = match[1];
                    var match = regex_eth0_ip.exec(str_output);
                    if (match)
                        player.eth0_ip = match[1];
                    var match = regex_wlan0_hw.exec(str_output);
                    if (match)
                        player.wlan0_mac = match[1];
                    var match = regex_wlan0_ip.exec(str_output);
                    if (match)
                        player.wlan0_ip = match[1];
                }
                break;
            case "process::stderr":
                Draw_1["default"].setCursor(0, 20);
                Draw_1["default"].setColor(1, 0, true);
                Draw_1["default"].paintText(Draw_1["default"].padString(data.stderr, 80));
                break;
            case "processes::update":
                return output("Process list has been updated");
            case "process::exit":
                if (player.currently_playing && data.process == player.currently_playing.process) {
                    self.player.next();
                    self.player.play();
                }
                break;
            case "player::play":
                return output("Playing " + (self.player.playing) + " / " + self.player.playlist.items.length + ": " + self.player.currently_playing.title);
            case "download::start":
                return output('Start downloading: ' + data.url_remote);
            case "download::progress":
                return output('Download ' + data.url_remote + ' has downloaded ' + data.progress + ' bytes');
            case "download::finish":
                if (data.filename == 'playlist.json') {
                }
                return output('Finished downloading: ' + data.url_remote);
            case "download::error":
                return output('Error downloading: ' + data.download.url_remote + ' because: ' + data.error);
            default:
        }
    };
    return PlayerLogic;
}());
Draw_1["default"].clear();
var player = PlayerBuilder_1.PlayerBuilder.build(player_json);
MessageEmitter_1["default"].addListener(player);
MessageEmitter_1["default"].addListener(new PlayerLogic(player));
ProcessManager_1["default"].start(net_process);
player.start();
var playlist_download = new Download_1["default"]();
playlist_download.url_remote = settings.playlist + '?' + player.eth0_mac;
playlist_download.folder_local = settings.cache;
playlist_download.name = 'Playlist meta file';
playlist_download.filename = 'playlist.json';
setInterval(function () {
    DownloadManager_1["default"].addDownload(playlist_download);
}, settings.playlist_refresh_rate);
player.playlist.items.forEach(function (item) {
    item.folder_local = settings.cache;
    DownloadManager_1["default"].addDownload(item);
});
DownloadManager_1["default"].start();
//# sourceMappingURL=test.js.map
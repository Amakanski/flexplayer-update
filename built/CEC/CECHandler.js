"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var spawn = require('child_process').spawn;
var CECHandler = (function () {
    function CECHandler() {
    }
    CECHandler.start = function () {
        MessageEmitter_1["default"].emit('debug', 'CECHandler is starting');
        var self = this;
        this.cec_client = spawn('cec-client');
        this.cec_client.on('error', function (error) {
            MessageEmitter_1["default"].emit('CECHandler::error', error);
        });
        this.cec_client.stdout.on('data', function (data) {
            var regex = /01:([0-9a-f]+):([0-9a-f]+)/;
            var match = String(data).match(regex);
            if (match != null && match[1] == self.CEC_PRESS) {
                self.available_commands.forEach(function (command) {
                    if (match[2] == command)
                        MessageEmitter_1["default"].emit("CECHandler", "on_command", command);
                });
            }
        });
        this.cec_client.on('close', function () {
            MessageEmitter_1["default"].emit('CECHandler', 'on_exit');
        });
    };
    CECHandler.send = function (message) {
        this.cec_client.stdin.write('00:00:00');
    };
    CECHandler.CEC_PRESS = '44';
    CECHandler.CEC_RELEASE = '8b';
    CECHandler.CEC_SELECT = '00';
    CECHandler.CEC_UP = '01';
    CECHandler.CEC_DOWN = '02';
    CECHandler.CEC_LEFT = '03';
    CECHandler.CEC_RIGHT = '04';
    CECHandler.CEC_SETUP = '0a';
    CECHandler.CEC_RETURN = '0d';
    CECHandler.CEC_PLAY = '44';
    CECHandler.CEC_STOP = '45';
    CECHandler.CEC_RECORD = '47';
    CECHandler.CEC_REWIND = '48';
    CECHandler.CEC_FORWARD = '49';
    CECHandler.available_commands = [
        CECHandler.CEC_SELECT,
        CECHandler.CEC_UP,
        CECHandler.CEC_DOWN,
        CECHandler.CEC_LEFT,
        CECHandler.CEC_RIGHT,
        CECHandler.CEC_SETUP,
        CECHandler.CEC_RETURN,
        CECHandler.CEC_PLAY,
        CECHandler.CEC_STOP,
        CECHandler.CEC_RECORD,
        CECHandler.CEC_REWIND,
        CECHandler.CEC_FORWARD
    ];
    return CECHandler;
}());
exports.__esModule = true;
exports["default"] = CECHandler;
//# sourceMappingURL=CECHandler.js.map
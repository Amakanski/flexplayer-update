"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var CECHandler_1 = require('../CEC/CECHandler');
var PlayerLogic_1 = require('../Main/PlayerLogic');
var RemoteLogic = (function () {
    function RemoteLogic(player) {
        this.player = player;
        MessageEmitter_1["default"].addListener(this);
    }
    RemoteLogic.prototype.on = function (source, message, data) {
        if (source == "main" && message == "on_initialize")
            CECHandler_1["default"].start();
        else if (source == "CECHandler" && message == "on_exit") {
            MessageEmitter_1["default"].emit('debug', 'CECHandler has exited unexpectedly');
            CECHandler_1["default"].start();
        }
        else if (source == "CECHandler" && message == "on_command")
            switch (data) {
                case CECHandler_1["default"].CEC_STOP:
                    PlayerLogic_1["default"].enabled = false;
                    this.player.stop();
                    break;
                case CECHandler_1["default"].CEC_PLAY:
                    PlayerLogic_1["default"].enabled = true;
                    this.player.play();
                    break;
                case CECHandler_1["default"].CEC_SETUP:
                    PlayerLogic_1["default"].enabled = false;
                    break;
                case CECHandler_1["default"].CEC_FORWARD:
                    if (PlayerLogic_1["default"].enabled) {
                        this.player.stop();
                    }
                    else {
                        PlayerLogic_1["default"].enabled = true;
                        this.player.next();
                    }
                    break;
                case CECHandler_1["default"].CEC_REWIND:
                    if (PlayerLogic_1["default"].enabled) {
                        this.player.play_index = this.player.play_index - 2;
                        if (this.player.play_index < 0)
                            this.player.play_index += this.player.device.playlist.nodes.length;
                        this.player.stop();
                    }
                    else {
                        PlayerLogic_1["default"].enabled = true;
                        this.player.previous();
                    }
                    break;
            }
    };
    return RemoteLogic;
}());
exports.__esModule = true;
exports["default"] = RemoteLogic;
//# sourceMappingURL=RemoteLogic.js.map
"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var DiskManager_1 = require('../Disk/DiskManager');
var DownloadManager_1 = require('../Download/DownloadManager');
var Download_1 = require('../Download/Download');
var Device_1 = require('../Player/Device');
var fs = require('fs');
var async = require('async');
var settings = require('../../settings.json');
var DownloadLogic = (function () {
    function DownloadLogic(player) {
        this.max_playlist_age = 10 * 60 * 1000;
        this.player = player;
        MessageEmitter_1["default"].addListener(this);
    }
    DownloadLogic.prototype.downloadPlaylist = function () {
    };
    DownloadLogic.prototype.checkPlaylist = function () {
        try {
            var data = fs.readFileSync(settings.cache_folder + settings.device_file, 'utf8');
            this.player.device = new Device_1["default"](JSON.parse(data));
        }
        catch (e) {
            MessageEmitter_1["default"].emit("debug", "Unable to build device");
            MessageEmitter_1["default"].emit("debug", e.message);
        }
        var that = this;
        that.player.device.playlist.nodes
            .filter(function (node) { return !node.is_available() && !node.downloading; })
            .forEach(function (node) {
            var path = 'cache/' + node.content.url + '.' + node.content.type;
            if (fs.existsSync(path) && fs.statSync(path).size != 0) {
                node.file_exists = true;
            }
            else {
                MessageEmitter_1["default"].emit('debug', 'File missing: ' + path);
                node.downloading = true;
                DownloadManager_1["default"].download(new Download_1.NodeDownload(that.player.device, node));
            }
        });
    };
    DownloadLogic.prototype.on = function (source, message, data) {
        var that = this;
        if (source == "main" && message == "on_initialize")
            this.checkPlaylist();
        if (source == "playlist_download" && message == "on_update")
            this.checkPlaylist();
        if (source == 'download' && message == 'on_complete') {
            DiskManager_1["default"].get_disk_usage()
                .then(function () {
                that.player.device.storage = DiskManager_1["default"].storage_total;
                that.player.device.storage_free = DiskManager_1["default"].storage_free;
                DownloadManager_1["default"].post_player_status(that.player);
            })
                .catch(function (error) { });
        }
    };
    return DownloadLogic;
}());
exports.__esModule = true;
exports["default"] = DownloadLogic;
//# sourceMappingURL=DownloadLogic.js.map
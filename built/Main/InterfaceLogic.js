"use strict";
var CECHandler_1 = require('../CEC/CECHandler');
var InterfaceHandler_1 = require('../Interface/InterfaceHandler');
var InterfaceBuilder_1 = require('../Interface/InterfaceBuilder');
var ListboxItem_1 = require('../Interface/ListboxItem');
var NetworkHandler_1 = require('../Network/NetworkHandler');
var ConfigHandler_1 = require('../Config/ConfigHandler');
var interface_json = require('../../interface.json');
var InterfaceLogic = (function () {
    function InterfaceLogic(player) {
        var _this = this;
        var self = this;
        self.player = player;
        InterfaceHandler_1["default"].bindRootNode(InterfaceBuilder_1["default"].build(interface_json));
        InterfaceHandler_1["default"].getElementById('main_container').size.x = InterfaceHandler_1["default"].getElementById('keyboard').size.x;
        InterfaceHandler_1["default"].setFocus(InterfaceHandler_1["default"].getElementById('main_container'));
        InterfaceHandler_1["default"].getElementById('keyboard').event_handler.addListener({
            "on_back": function () { InterfaceHandler_1["default"].setFocus(InterfaceHandler_1["default"].getElementById('main_container')); }
        });
        InterfaceHandler_1["default"].getElementById('button_connect').event_handler.addListener({
            "on_click": function () {
                var ssid = InterfaceHandler_1["default"].getElementById('input_ssid').value;
                var pw = InterfaceHandler_1["default"].getElementById('input_password').value;
                NetworkHandler_1["default"].connectToSSID(NetworkHandler_1["default"].nic_wlan0, ssid, pw, {
                    on_status: function (status) { InterfaceHandler_1["default"].getElementById('status').setValue(status); },
                    on_error: function (error) { InterfaceHandler_1["default"].getElementById('status').setValue(error); },
                    on_success: function (msg) { InterfaceHandler_1["default"].getElementById('status').setValue(msg); }
                });
            }
        });
        ['input_mac', 'input_password', 'input_ip', 'input_subnet', 'input_gateway', 'input_dns1', 'input_dns2']
            .forEach(function (name) {
            InterfaceHandler_1["default"].bindKeyboard(InterfaceHandler_1["default"].getElementById('keyboard'), InterfaceHandler_1["default"].getElementById(name));
        });
        InterfaceHandler_1["default"].getElementById('checkbox_wifi').event_handler.addListener({
            "on_change": function () {
                if (InterfaceHandler_1["default"].getElementById('checkbox_wifi').checked) {
                    [
                        ['label_mac', 'WiFi MAC  '],
                        ['label_instellingen', 'WiFi IP Instellingen'],
                        ['input_mac', NetworkHandler_1["default"].nic_wlan0.mac],
                        ['input_ip', NetworkHandler_1["default"].nic_wlan0.ip],
                        ['input_subnet', NetworkHandler_1["default"].nic_wlan0.subnet],
                        ['input_gateway', NetworkHandler_1["default"].nic_wlan0.gateway],
                        ['input_dns1', NetworkHandler_1["default"].nic_wlan0.dns1],
                        ['input_dns2', NetworkHandler_1["default"].nic_wlan0.dns2]
                    ]
                        .forEach(function (p) {
                        InterfaceHandler_1["default"].getElementById(p[0]).setValue(p[1]);
                    });
                    [
                        'input_ssid',
                        'input_password',
                        'button_connect'
                    ]
                        .forEach(function (name) {
                        InterfaceHandler_1["default"].getElementById(name).setReadonly(false);
                    });
                }
                else {
                    [
                        ['label_mac', 'LAN MAC  '],
                        ['label_instellingen', 'LAN IP Instellingen '],
                        ['input_mac', NetworkHandler_1["default"].nic_eth0.mac],
                        ['input_ip', NetworkHandler_1["default"].nic_eth0.ip],
                        ['input_subnet', NetworkHandler_1["default"].nic_eth0.subnet],
                        ['input_gateway', NetworkHandler_1["default"].nic_eth0.gateway],
                        ['input_dns1', NetworkHandler_1["default"].nic_eth0.dns1],
                        ['input_dns2', NetworkHandler_1["default"].nic_eth0.dns2]
                    ]
                        .forEach(function (p) {
                        InterfaceHandler_1["default"].getElementById(p[0]).setValue(p[1]);
                    });
                    [
                        'input_ssid',
                        'input_password',
                        'button_connect'
                    ]
                        .forEach(function (e) {
                        InterfaceHandler_1["default"].getElementById(e).setReadonly(true);
                    });
                }
            }
        });
        InterfaceHandler_1["default"].getElementById('checkbox_wifi').setChecked(ConfigHandler_1["default"].get('use_wifi') == "true");
        InterfaceHandler_1["default"].getElementById('checkbox_dhcp').event_handler.addListener({
            "on_change": function () {
                [
                    'input_ip',
                    'input_subnet',
                    'input_gateway',
                    'input_dns1',
                    'input_dns2'
                ]
                    .forEach(function (name) {
                    InterfaceHandler_1["default"].getElementById(name).setReadonly(InterfaceHandler_1["default"].getElementById('checkbox_dhcp').checked);
                });
            }
        });
        InterfaceHandler_1["default"].getElementById('checkbox_dhcp').setChecked(ConfigHandler_1["default"].get('dhcp') == "true");
        InterfaceHandler_1["default"].getElementById('input_ssid').event_handler.addListener({
            "on_click": function () {
                var count = 0;
                InterfaceHandler_1["default"].getElementById('ssid_list').children.length = 0;
                InterfaceHandler_1["default"].getElementById('status').setValue('Bezig met scannen naar WiFi signalen...');
                InterfaceHandler_1["default"].setFocus(InterfaceHandler_1["default"].getElementById('ssid_list'));
                NetworkHandler_1["default"].scan(NetworkHandler_1["default"].nic_wlan0, {
                    onSuccess: function (list) {
                        var filter = [];
                        var addToArr = function (ssid) {
                            for (var i = 0; i < filter.length; i++)
                                if (filter[i].SSID == ssid.SSID) {
                                    if (filter[i].signal < ssid.signal)
                                        filter[i] = ssid;
                                    return;
                                }
                            filter.push(ssid);
                        };
                        list
                            .sort(function (ssid_a, ssid_b) {
                            return ssid_a.signal < ssid_b.signal;
                        })
                            .forEach(addToArr);
                        filter
                            .forEach(function (ssid) {
                            var lbi = new ListboxItem_1["default"]({ id: ssid.SSID, value: ssid.SSID + ' (' + ssid.signal + ')' });
                            lbi.event_handler.addListener({
                                "on_click": function () {
                                    InterfaceHandler_1["default"].setFocus(InterfaceHandler_1["default"].getElementById('main_container'));
                                    InterfaceHandler_1["default"].getElementById('input_ssid').setValue(lbi.id);
                                }
                            });
                            InterfaceHandler_1["default"].getElementById('ssid_list').addChild(lbi);
                        });
                        if (InterfaceHandler_1["default"].getElementById('ssid_list').children.length > 0) {
                            InterfaceHandler_1["default"].getElementById('ssid_list').child_focus = InterfaceHandler_1["default"].getElementById('ssid_list').children[0];
                            InterfaceHandler_1["default"].getElementById('ssid_list').child_focus.focus = true;
                        }
                        else {
                            InterfaceHandler_1["default"].getElementById('status').setValue('Geen WiFi signalen gevonden. Is een WiFi dongel aangesloten?');
                        }
                        InterfaceHandler_1["default"].getElementById('status').setValue('                                                             ');
                        InterfaceHandler_1["default"].getElementById('ssid_list').show();
                    },
                    onError: function (error) {
                        InterfaceHandler_1["default"].getElementById('status').setValue(error);
                    }
                });
            },
            "on_back": function () {
            }
        });
        InterfaceHandler_1["default"].getElementById('button_save').event_handler.addListener({
            "on_click": function () {
                NetworkHandler_1["default"].updateConfig({
                    wifi: InterfaceHandler_1["default"].getElementById('checkbox_dhcp').checked,
                    dhcp: InterfaceHandler_1["default"].getElementById('checkbox_wifi').checked,
                    ip: InterfaceHandler_1["default"].getElementById('input_ip').value,
                    subnet: InterfaceHandler_1["default"].getElementById('input_subnet').value,
                    gateway: InterfaceHandler_1["default"].getElementById('input_gateway').value,
                    dns1: InterfaceHandler_1["default"].getElementById('input_dns1').value,
                    dns2: InterfaceHandler_1["default"].getElementById('input_dns2').value
                });
                InterfaceHandler_1["default"].interfacing = false;
                _this.player.play();
            }
        });
        InterfaceHandler_1["default"].getElementById('button_cancel').event_handler.addListener({
            "on_click": function () {
                InterfaceHandler_1["default"].interfacing = false;
                _this.player.play();
            }
        });
        NetworkHandler_1["default"].listeners.push({
            "onUpdate": function (device, message) {
                if (!InterfaceHandler_1["default"].getElementById('checkbox_wifi').checked == device.wired) {
                    InterfaceHandler_1["default"].getElementById('input_mac').setValue(device.mac);
                    InterfaceHandler_1["default"].getElementById('input_ip').setValue(device.ip);
                    InterfaceHandler_1["default"].getElementById('input_subnet').setValue(device.subnet);
                    InterfaceHandler_1["default"].getElementById('input_gateway').setValue(device.gateway);
                    InterfaceHandler_1["default"].getElementById('input_dns1').setValue(device.dns1);
                    InterfaceHandler_1["default"].getElementById('input_dns2').setValue(device.dns2);
                }
                if (!device.wired) {
                    InterfaceHandler_1["default"].getElementById('input_ssid').setValue(device.ssid);
                }
            }
        });
        NetworkHandler_1["default"].getConfig(NetworkHandler_1["default"].nic_wlan0);
        NetworkHandler_1["default"].getConfig(NetworkHandler_1["default"].nic_eth0);
    }
    InterfaceLogic.prototype.on = function (message, data) {
        if (message == "CECHandler::command")
            switch (data) {
                case CECHandler_1["default"].CEC_SETUP:
                    if (!InterfaceHandler_1["default"].interfacing) {
                        InterfaceHandler_1["default"].interfacing = true;
                        this.player.stop();
                        NetworkHandler_1["default"].getConfig(NetworkHandler_1["default"].nic_eth0);
                        NetworkHandler_1["default"].getConfig(NetworkHandler_1["default"].nic_wlan0);
                        InterfaceHandler_1["default"].root_node.show();
                    }
                    break;
                case CECHandler_1["default"].CEC_RETURN:
                    if (InterfaceHandler_1["default"].interfacing) {
                        InterfaceHandler_1["default"].interfacing = false;
                        InterfaceHandler_1["default"].root_node.hide();
                        this.player.play();
                    }
                case CECHandler_1["default"].CEC_UP:
                    if (InterfaceHandler_1["default"].interfacing)
                        InterfaceHandler_1["default"].command('up');
                    break;
                case CECHandler_1["default"].CEC_LEFT:
                    if (InterfaceHandler_1["default"].interfacing)
                        InterfaceHandler_1["default"].command('left');
                    break;
                case CECHandler_1["default"].CEC_RIGHT:
                    if (InterfaceHandler_1["default"].interfacing)
                        InterfaceHandler_1["default"].command('right');
                    break;
                case CECHandler_1["default"].CEC_DOWN:
                    if (InterfaceHandler_1["default"].interfacing)
                        InterfaceHandler_1["default"].command('down');
                    break;
                case CECHandler_1["default"].CEC_SELECT:
                    if (InterfaceHandler_1["default"].interfacing)
                        InterfaceHandler_1["default"].command('click');
                    break;
            }
    };
    return InterfaceLogic;
}());
exports.__esModule = true;
exports["default"] = InterfaceLogic;
//# sourceMappingURL=InterfaceLogic.js.map
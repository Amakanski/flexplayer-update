"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var PlayerLogic = (function () {
    function PlayerLogic(player) {
        this.player = player;
        MessageEmitter_1["default"].addListener(this);
    }
    PlayerLogic.prototype.on = function (source, message, data) {
        if (source == "main" && message == "on_initialize" && PlayerLogic.enabled)
            this.player.play();
        if (source == "player" && message == "on_finish" && PlayerLogic.enabled)
            this.player.next();
        if (source == "player" && message == "on_error" && PlayerLogic.enabled) {
            this.player.next();
        }
    };
    PlayerLogic.enabled = true;
    return PlayerLogic;
}());
exports.__esModule = true;
exports["default"] = PlayerLogic;
//# sourceMappingURL=PlayerLogic.js.map
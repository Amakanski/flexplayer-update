"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var start_time = new Date().getTime();
var VerboseLogic = (function () {
    function VerboseLogic(player) {
        this.player = player;
        MessageEmitter_1["default"].addListener(this);
    }
    VerboseLogic.prototype.on = function (source, message, data) {
        if (source == "debug")
            VerboseLogic.lines.push(message);
    };
    VerboseLogic.lines = new Array();
    return VerboseLogic;
}());
exports.__esModule = true;
exports["default"] = VerboseLogic;
//# sourceMappingURL=VerboseLogic.js.map
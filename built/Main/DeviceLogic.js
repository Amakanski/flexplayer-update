"use strict";
var NetworkHandler_1 = require('../Network/NetworkHandler');
var DownloadManager_1 = require('../Download/DownloadManager');
var Download_1 = require('../Download/Download');
var fs = require('fs');
var settings = require('../../settings.json');
var DeviceLogic = (function () {
    function DeviceLogic(player) {
        this.device_config = 'cache/device.json';
        this.create_device_download = function () {
            var device_download = new Download_1["default"]();
            device_download.name = 'device';
            device_download.from = 'http://' + settings.server + '/data/devices/?mac=' + NetworkHandler_1["default"].nic_eth0.mac + '&ip=' + NetworkHandler_1["default"].nic_eth0.ip;
            device_download.to = this.device_config;
            device_download.filename = '';
            device_download.overwrite = true;
            DownloadManager_1["default"].addDownload(device_download);
        };
        this.player = player;
    }
    DeviceLogic.prototype.on = function (source, message, data) {
        var _this = this;
        if (source == 'device' && message == 'update') {
            var d = new Download_1["default"]();
            d.from = 'http://';
            DownloadManager_1["default"].addDownload(d);
        }
        switch (source) {
            case "device::error_reading_device_config":
                this.create_device_download();
                break;
            case "device::update":
                this.player.device.playlist.nodes.forEach(function (item) {
                    console.log('Adding download: ', item.content.title);
                    var d = new Download_1["default"]();
                    d.url_remote = 'http://' + settings.server + '/stream/' + item.content.url + '/' + _this.player.device.id + '/';
                    d.folder_local = 'cache/' + item.content.url + '.' + item.content.type;
                    DownloadManager_1["default"].addDownload(d);
                });
                break;
        }
    };
    return DeviceLogic;
}());
exports.__esModule = true;
exports["default"] = DeviceLogic;
//# sourceMappingURL=DeviceLogic.js.map
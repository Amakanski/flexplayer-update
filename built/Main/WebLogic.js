"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var WebHandler_1 = require('../Web/WebHandler');
var DownloadManager_1 = require('../Download/DownloadManager');
var NetworkHandler_1 = require('../Network/NetworkHandler');
var settings = require('../../settings.json');
var querystring = require('querystring');
var WebLogic = (function () {
    function WebLogic(player) {
        this.player = player;
        MessageEmitter_1["default"].addListener(this);
    }
    WebLogic.prototype.on = function (source, message, data) {
        if (source == "main" && message == "on_initialize")
            WebHandler_1["default"].startServer();
        else if (source == "web" && message == "on_get") {
            var url = data.url;
            var response = data.response;
            switch (url) {
                case '/player/next/':
                    this.player.next();
                    break;
                case '/statusp/':
                    var str = data.request.url.split('?');
                    var qstring = querystring.parse(str[1]);
                    WebHandler_1["default"].serveJSONP(qstring['callback'], {
                        device: this.player.device,
                        playing: this.player.playing,
                        downloading: DownloadManager_1["default"].downloading,
                        download_queue: DownloadManager_1["default"].download_queue
                    }, response);
                    break;
                case '/status/':
                    WebHandler_1["default"].serveJSON({
                        device: this.player.device,
                        playing: this.player.playing,
                        downloading: DownloadManager_1["default"].downloading,
                        download_queue: DownloadManager_1["default"].download_queue
                    }, response);
                    break;
                case '/config/' + NetworkHandler_1["default"].nic_eth0.name + '/':
                    NetworkHandler_1["default"].getConfig(NetworkHandler_1["default"].nic_eth0, function () {
                        WebHandler_1["default"].serveJSON(NetworkHandler_1["default"].nic_eth0, response);
                    });
                    break;
                case '/config/' + NetworkHandler_1["default"].nic_wlan0.name + '/':
                    NetworkHandler_1["default"].getConfig(NetworkHandler_1["default"].nic_wlan0, function () {
                        WebHandler_1["default"].serveJSON(NetworkHandler_1["default"].nic_wlan0, response);
                    });
                    break;
                case '/config/':
                    WebHandler_1["default"].serveJSON(settings, response);
                case '/wifi/':
                    var filter = [];
                    var respond = function () { WebHandler_1["default"].serveJSON(filter, response); };
                    NetworkHandler_1["default"].scan(NetworkHandler_1["default"].nic_wlan0, {
                        onSuccess: function (list) {
                            var addToArr = function (ssid) {
                                for (var i = 0; i < filter.length; i++)
                                    if (filter[i].SSID == ssid.SSID) {
                                        if (filter[i].signal < ssid.signal)
                                            filter[i] = ssid;
                                        return;
                                    }
                                filter.push(ssid);
                            };
                            list
                                .sort(function (ssid_a, ssid_b) {
                                return ssid_a.signal < ssid_b.signal;
                            })
                                .forEach(addToArr);
                            respond();
                        },
                        onError: function (error) {
                            filter.push(error);
                            respond();
                        }
                    });
                    break;
                default:
                    WebHandler_1["default"].serveFile(url, response);
            }
        }
        else if (source == "web" && message == "on_post") {
            MessageEmitter_1["default"].emit("debug", "POST: " + data);
            var url = data.url;
            var response = data.response;
            if (url == '/config/') {
                var body = '';
                data.request.on('data', function (data) { body += data; });
                data.request.on('end', function () {
                    response.writeHead(200, { 'Content-Type': 'text/html' });
                    try {
                        var json = JSON.parse(body);
                        NetworkHandler_1["default"].updateConfig(json);
                        response.write('Updated');
                    }
                    catch (e) {
                        response.write(body + "\n" + e.message);
                    }
                    response.end();
                });
            }
            else if (url == '/connect_wifi/') {
                var body = '';
                data.request.on('data', function (data) { body += data; });
                data.request.on('end', function () {
                    var json = JSON.parse(body);
                    var ssid = json.ssid;
                    var password = json.password;
                    response.writeHead(200, { 'Content-Type': 'text/html' });
                    NetworkHandler_1["default"].connectToSSID(NetworkHandler_1["default"].nic_wlan0, ssid, password, {
                        on_status: function (status) { response.write(status + "\n"); },
                        on_error: function (error) { response.write(error + "\n"); response.end(); },
                        on_success: function (msg) { response.write(msg + "\n"); response.end(); }
                    });
                });
            }
            else {
                response.writeHead(500, { 'Content-Type': 'text/html' });
                response.write('Illegal POST');
                response.end();
            }
        }
    };
    return WebLogic;
}());
exports.__esModule = true;
exports["default"] = WebLogic;
//# sourceMappingURL=WebLogic.js.map
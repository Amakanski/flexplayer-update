"use strict";
var DebugView_1 = require('../Render/DebugView');
var DownloadView_1 = require('../Render/DownloadView');
var PlayerView_1 = require('../Render/PlayerView');
var RenderLogic = (function () {
    function RenderLogic(player) {
        this.downloadview_box = [1, 2, 100, 15];
        this.playerview_box = [1, 17, 100, 35];
        this.debugview_box = [102, 2, 135, 50];
        this.interfaceview_box = [0, 0, 130, 50];
        this.fps = 5;
        this.player = player;
        this.paint();
    }
    RenderLogic.prototype.paint = function () {
        DownloadView_1["default"].render(this.downloadview_box);
        PlayerView_1["default"].render(this.playerview_box, this.player);
        DebugView_1["default"].render(this.debugview_box);
        var that = this;
        setTimeout(function () { that.paint(); }, 1000 / that.fps);
    };
    RenderLogic.prototype.on = function (source, message, data) {
    };
    return RenderLogic;
}());
exports.__esModule = true;
exports["default"] = RenderLogic;
//# sourceMappingURL=RenderLogic.js.map
"use strict";
var MessageEmitter_1 = require("../Message/MessageEmitter");
var fs = require('fs');
var settings = require('../../settings.json');
var PlaylistLogic = (function () {
    function PlaylistLogic(player) {
        this.player = player;
    }
    PlaylistLogic.prototype.on = function (message, data) {
        switch (message) {
            case "main::initialize":
                break;
            case "playlist::error_reading_playlist":
            case "playlist::download":
                break;
            case "download::start":
                break;
            case "download::progress":
                break;
            case "download::error":
                break;
            case "download::finish":
                break;
            case "player::playlist_finished":
                MessageEmitter_1["default"].emit('debug', 'Playlist finished');
                break;
        }
    };
    PlaylistLogic.device_cache_file = "cache/device.json";
    return PlaylistLogic;
}());
exports.__esModule = true;
exports["default"] = PlaylistLogic;
//# sourceMappingURL=PlaylistLogic.js.map
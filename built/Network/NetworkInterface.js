"use strict";
var NetworkInterface = (function () {
    function NetworkInterface(name) {
        this.name = '';
        this.ip = '';
        this.mac = '';
        this.subnet = '';
        this.gateway = '';
        this.dns1 = '';
        this.dns2 = '';
        this.name = name;
    }
    return NetworkInterface;
}());
exports.__esModule = true;
exports["default"] = NetworkInterface;
//# sourceMappingURL=NetworkInterface.js.map
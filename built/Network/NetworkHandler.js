"use strict";
var WiredInterface_1 = require('./WiredInterface');
var WirelessInterface_1 = require('./WirelessInterface');
var ConfigHandler_1 = require('../Config/ConfigHandler');
var WebHandler_1 = require('../Web/WebHandler');
var child_process = require('child_process');
var util = require('util');
var fs = require('fs');
var NetworkHandler = (function () {
    function NetworkHandler() {
    }
    NetworkHandler.trigger = function (device, trigger, message) {
        this.listeners.forEach(function (l) {
            if (typeof l[trigger] == 'function')
                l[trigger](device, message);
        });
    };
    NetworkHandler.getConfig = function (device, callback) {
        var self = this;
        var spawn = child_process.spawn('ifconfig', [device.name]);
        var data = '';
        spawn.stdout.on('data', function (chunk) {
            data += chunk;
        });
        spawn.on('error', function (error) {
            NetworkHandler.trigger(device, 'onError', 'Could not run ifconfig on interface: ' + device.name + ', ' + error);
        });
        spawn.on('close', function () {
            var regex_hw = /HWaddr ([0-9a-f:]+)/;
            var match = regex_hw.exec(data);
            device.mac = match ? match[1] : '';
            var regex_ip = /inet addr:([0-9\.]+)/;
            var match = regex_ip.exec(data);
            device.ip = match ? match[1] : '';
            var regex_mask = /Mask:([0-9\.]+)/;
            var match = regex_mask.exec(data);
            device.subnet = match ? match[1] : '';
            NetworkHandler.trigger(device, 'onUpdate', 'Retrieved IP and MAC address');
            self.readRoute(device, callback);
        });
    };
    NetworkHandler.readRoute = function (device, callback) {
        var self = this;
        var data = "";
        var spawn = child_process.spawn("ip", ["route"]);
        spawn.stdout.on('data', function (chunk) {
            data += chunk;
        });
        spawn.on('error', function (error) {
            NetworkHandler.trigger(device, 'onError', 'Error reading route for ' + device.name);
        });
        spawn.on('close', function () {
            var str = "default via ([0-9\\.]+) dev " + device.name;
            var r = RegExp(str, "m");
            var match = r.exec(data);
            device.gateway = match ? match[1] : '';
            self.readDNS(device, callback);
        });
    };
    NetworkHandler.readDNS = function (device, callback) {
        var self = this;
        var data = "";
        var result = Array();
        var spawn = child_process.spawn('cat', ["/etc/resolv.conf"]);
        spawn.stdout.on('data', function (chunk) {
            data += chunk;
        });
        spawn.on('error', function (error) {
            NetworkHandler.trigger(device, 'onError', 'Could not read /etc/resolv.conf');
        });
        spawn.on('close', function () {
            var dns = /nameserver ([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})/g;
            var match;
            while (match = dns.exec(data)) {
                result.push(match[1]);
            }
            device.dns1 = result[0] ? result[0] : '';
            device.dns2 = result[1] ? result[1] : '';
            NetworkHandler.trigger(device, 'onUpdate', 'DNS changed');
            if (!device.wired)
                self.readWireless(device, callback);
            else if (typeof callback == 'function')
                callback();
        });
    };
    NetworkHandler.readWireless = function (device, callback) {
        var self = this;
        var data = "";
        var spawn = child_process.spawn('iwconfig', [device.name]);
        spawn.stdout.on('data', function (chunk) { data += chunk; });
        spawn.on('error', function (error) {
            NetworkHandler.trigger(device, 'onError', 'Could not read from iwconfig ' + device.name);
        });
        spawn.on('close', function () {
            var regex = /ESSID:"([^"]+)"/g;
            var match = regex.exec(data);
            if (match)
                device.ssid = match[1];
            else
                device.ssid = '';
            NetworkHandler.trigger(device, 'onUpdate', 'SSID changed');
            if (typeof callback == 'function')
                callback();
        });
    };
    NetworkHandler.up = function (device) {
        var spawn = child_process.spawn('ifconfig', [device.name, 'up']);
        NetworkHandler.trigger(device, 'onUpdate', 'Network ' + device.name + ' is going up');
    };
    NetworkHandler.down = function (device) {
        var spawn = child_process.spawn('ifconfig', [device.name, 'down']);
        NetworkHandler.trigger(device, 'onUpdate', 'Network ' + device.name + ' is going down');
    };
    NetworkHandler.scan = function (device, callback) {
        if (device.wired)
            throw new Error('Cannot scan wired connection ' + device.name);
        if (device.simulation) {
            callback.onSuccess([
                {
                    'signal': '-20',
                    'SSID': 'Anyflex'
                },
                {
                    'signal': '-45',
                    'SSID': 'Een andere SSID'
                }
            ]);
            return true;
        }
        var scan_spawn = child_process.spawn('iw', ['dev', device.name, 'scan']);
        var scan_data;
        var list = Array();
        scan_spawn.stdout.on('data', function (data) {
            scan_data += data;
        });
        scan_spawn.on('error', function (error) {
            callback.onError(error);
        });
        scan_spawn.on('close', function () {
            var regex = /signal: ([-\d\.]+).*\n\slast seen: ([\d]+).*\n.*\n\sSSID: ([^\n]+)/g;
            var matches;
            var count = 0;
            while ((matches = regex.exec(scan_data)) != null)
                list.push({
                    'signal': parseInt(matches[1]),
                    'SSID': matches[3]
                });
            callback.onSuccess(list);
        });
    };
    NetworkHandler.updateInterface = function (device) {
        var self = this;
        if (!device.wired)
            throw new Error('Use connection_info only on wirless devices');
        var connection_info_spawn = child_process.spawn('iw', ['dev', device.name, 'link']);
        var connection_info;
        connection_info_spawn.stdout.on('data', function (data) {
            connection_info += data;
        });
        connection_info_spawn.on('error', function (error) {
            NetworkHandler.trigger(this, 'onError', error);
        });
        connection_info_spawn.on('close', function () {
            var regex = /SSID: ([^\n]+)/;
            var match = String(connection_info).match(regex);
            if (match != null) {
                device.ssid = match[1];
                NetworkHandler.trigger(this, 'onUpdate', 'SSID update');
            }
            else
                NetworkHandler.trigger(this, 'onError', 'Not connected');
        });
    };
    NetworkHandler.connectToSSID = function (device, ssid, passcode, callback) {
        if (passcode.length < 8)
            return callback.on_error('Wachtwoord is te kort (minimaal 8 tekens)');
        if (passcode.length > 63)
            return callback.on_error('Wachtwoord is te lang (maximaal 63 tekens)');
        child_process.exec('killall wpa_supplicant');
        var config_spawn = child_process.spawn('wpa_passphrase', ['"' + ssid + '"', '"' + passcode + '"']);
        var config;
        callback.on_status('Creating config file');
        config_spawn.stdout.on('data', function (data) {
            config += data;
        });
        config_spawn.on('error', function (error) {
            callback.on_error(error);
        });
        var self = this;
        config_spawn.on('close', function () {
            var regex = /\spsk=([^\n]+)/;
            var match = String(config).match(regex);
            if (match != null) {
                var psk = match[1];
                var config_file = '/etc/wpa_supplicant/wpa_supplicant.conf';
                var config_file_string = util.format(self.config_file_template, ssid, passcode);
                callback.on_status('Writing config file');
                try {
                    fs.unlinkSync("/var/run/wpa_supplicant");
                    fs.unlinkSync(config_file);
                }
                catch (e) { }
                fs.writeFile(config_file, config_file_string, function (err) {
                    if (err)
                        callback.on_error('Unable to write config file');
                    else {
                        callback.on_status('Initializing wpa_supplicant');
                        var wpa_supplicant = child_process.spawn('wpa_supplicant', ['-i' + device.name, '-c' + config_file, '-B'], { detached: true });
                        var response = "";
                        wpa_supplicant.stdout.on('data', function (data) {
                            response += data;
                        });
                        wpa_supplicant.on('error', function (error) {
                            callback.on_error(error);
                        });
                        wpa_supplicant.on('close', function () {
                            callback.on_status('Retrieving IP address');
                            var ip_spawn = child_process.spawn('dhclient', [device.name]);
                            var ip_spawn_response;
                            ip_spawn.stdout.on('data', function (data) {
                                ip_spawn_response += data;
                            });
                            ip_spawn.on('error', function (error) {
                                callback.on_error(error);
                            });
                            ip_spawn.on('close', function () {
                                NetworkHandler.getConfig(device);
                                callback.on_success();
                            });
                        });
                    }
                });
            }
            else {
                callback.on_error('Unable to parse PSK');
            }
        });
    };
    NetworkHandler.updateConfig = function (data, callback) {
        WebHandler_1["default"].stopServer();
        ConfigHandler_1["default"].set('dhcp', data.dhcp);
        ConfigHandler_1["default"].save();
        var interfacing = "# IP Config template for Anyflex Flexplayer\n\n\# Loopback interface\n\auto lo\niface lo inet loopback\n";
        if (!data.wifi) {
            interfacing += "#auto " + NetworkHandler.nic_eth0.name + "\nallow-hotplug " + NetworkHandler.nic_eth0.name + "\n";
            if (data.dhcp)
                interfacing += "iface " + NetworkHandler.nic_eth0.name + " inet dhcp\n";
            else {
                interfacing += "iface " + NetworkHandler.nic_eth0.name + " inet static\n";
                interfacing += "	address " + data.ip + "\n";
                interfacing += "	netmask " + data.subnet + "\n";
                interfacing += "	gateway " + data.gateway + "\n";
                interfacing += "	dns-nameservers " + data.dns1 + " " + data.dns2 + "\n";
                interfacing += "	dns-search local\n";
            }
            interfacing += "#auto " + NetworkHandler.nic_wlan0.name + "\nallow-hotplug " + NetworkHandler.nic_wlan0.name + "\n";
            interfacing += "iface " + NetworkHandler.nic_wlan0.name + " inet static\n";
            interfacing += "    address 0.0.0.0\n";
        }
        else {
            interfacing += "#auto " + NetworkHandler.nic_wlan0.name + "\nallow-hotplug " + NetworkHandler.nic_wlan0.name + "\n";
            if (data.dhcp)
                interfacing += "iface " + NetworkHandler.nic_wlan0.name + " inet dhcp\n";
            else {
                interfacing += "iface " + NetworkHandler.nic_wlan0.name + " inet static\n";
                interfacing += "	address " + data.ip + "\n";
                interfacing += "	netmask " + data.subnet + "\n";
                interfacing += "	gateway " + data.gateway + "\n";
                interfacing += "	dns-nameservers " + data.dns1 + " " + data.dns2 + "\n";
                interfacing += "	dns-search local\n";
            }
            interfacing += "wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf\n";
            interfacing += "#auto " + NetworkHandler.nic_eth0.name + "\nallow-hotplug " + NetworkHandler.nic_eth0.name + "\n";
            interfacing += "iface " + NetworkHandler.nic_eth0.name + " inet dhcp\n";
        }
        var do_callback = function () {
            WebHandler_1["default"].startServer();
            if (typeof callback == 'function')
                callback();
        };
        child_process.exec('killall wpa_supplicant && ifdown ' + NetworkHandler.nic_wlan0.name + ' ' + NetworkHandler.nic_eth0.name, function () {
            fs.writeFileSync('/etc/network/interfaces', interfacing);
            child_process.exec('ifup ' + NetworkHandler.nic_wlan0.name + ' ' + NetworkHandler.nic_eth0.name, do_callback);
        });
    };
    NetworkHandler.simulation = true;
    NetworkHandler.listeners = new Array();
    NetworkHandler.nic_eth0 = new WiredInterface_1["default"]("eth0");
    NetworkHandler.nic_wlan0 = new WirelessInterface_1["default"]("wlan0");
    NetworkHandler.config_file_template = "\
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n\
update_config=1\n\
\n\
network={\n\
ssid=\"%s\"\n\
psk=\"%s\"\n\
proto=RSN\n\
key_mgmt=WPA-PSK\n\
pairwise=CCMP\n\
auth_alg=OPEN\n\
}\
\
";
    return NetworkHandler;
}());
exports.__esModule = true;
exports["default"] = NetworkHandler;
//# sourceMappingURL=NetworkHandler.js.map
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var NetworkInterface_1 = require('./NetworkInterface');
var WirelessInterface = (function (_super) {
    __extends(WirelessInterface, _super);
    function WirelessInterface(name) {
        _super.call(this, name);
        this.ssid = '';
        this.passcode = '';
        this.encrypt_type = 'aes';
        this.wired = false;
    }
    return WirelessInterface;
}(NetworkInterface_1["default"]));
exports.__esModule = true;
exports["default"] = WirelessInterface;
//# sourceMappingURL=WirelessInterface.js.map
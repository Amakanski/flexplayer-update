"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var NetworkInterface_1 = require('./NetworkInterface');
var WiredInterface = (function (_super) {
    __extends(WiredInterface, _super);
    function WiredInterface(name) {
        _super.call(this, name);
        this.wired = true;
    }
    return WiredInterface;
}(NetworkInterface_1["default"]));
exports.__esModule = true;
exports["default"] = WiredInterface;
//# sourceMappingURL=WiredInterface.js.map
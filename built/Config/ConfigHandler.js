"use strict";
var fs = require('fs');
var ConfigHandler = (function () {
    function ConfigHandler() {
    }
    ConfigHandler.set = function (parameter, value) {
        if (!this.loaded)
            this.load();
        this.config[parameter] = value;
    };
    ConfigHandler.get = function (parameter) {
        if (!this.loaded)
            this.load();
        return this.config[parameter];
    };
    ConfigHandler.list = function () {
        return this.config;
    };
    ConfigHandler.load = function () {
        try {
            this.config = JSON.parse(fs.readFileSync(this.file, 'utf8'));
            this.loaded = true;
        }
        catch (e) {
            console.log(e);
            process.exit(-1);
        }
    };
    ConfigHandler.save = function () {
        fs.writeFileSync(this.file, JSON.stringify(this.config));
    };
    ConfigHandler.file = './settings.json';
    ConfigHandler.loaded = false;
    return ConfigHandler;
}());
exports.__esModule = true;
exports["default"] = ConfigHandler;
//# sourceMappingURL=ConfigHandler.js.map
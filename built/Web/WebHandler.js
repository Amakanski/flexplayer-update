"use strict";
var MessageEmitter_1 = require('../Message/MessageEmitter');
var http = require('http');
var url = require('url');
var path = require('path');
var fs = require('fs');
var WebHandler = (function () {
    function WebHandler() {
    }
    WebHandler.startServer = function () {
        MessageEmitter_1["default"].emit("debug", "Starting webserver");
        this.server = http.createServer(this.handleRequest).listen(80);
    };
    WebHandler.stopServer = function () {
        this.server.close();
    };
    WebHandler.handleRequest = function (request, response) {
        if (request.method == 'POST')
            MessageEmitter_1["default"].emit('web', 'on_post', { url: url.parse(request.url).pathname, request: request, response: response });
        else
            MessageEmitter_1["default"].emit('web', 'on_get', { url: url.parse(request.url).pathname, request: request, response: response });
    };
    WebHandler.serveJSON = function (data, response) {
        response.writeHead(200, { "Content-Type": "text/json" });
        response.write(JSON.stringify(data));
        response.end();
    };
    WebHandler.serveJSONP = function (callback, data, response) {
        response.writeHead(200, { "Content-Type": "application/javascript" });
        response.write(callback + '(' + JSON.stringify(data) + ');');
        response.end();
    };
    WebHandler.serveFile = function (filename, response) {
        if (filename == '/')
            filename = 'status.html';
        fs.readFile('/home/pi/webapp/' + filename, 'binary', function (err, file) {
            if (err) {
                response.writeHead(500, { "Content-Type": "text/plain" });
                response.write('Message:' + err + "\n");
                response.write('Path: ' + path.dirname(fs.realpathSync(__filename)) + "\n");
                response.write('Request: ' + filename + "\n");
                response.end();
                return;
            }
            response.writeHead(200);
            response.write(file, "binary");
            response.end();
        });
    };
    return WebHandler;
}());
exports.__esModule = true;
exports["default"] = WebHandler;
//# sourceMappingURL=WebHandler.js.map
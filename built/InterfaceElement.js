"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var InterfaceEventEmitter = (function () {
    function InterfaceEventEmitter() {
    }
    InterfaceEventEmitter.prototype.emit = function (trigger, data) {
        this.listeners.forEach(function (il) {
            il.on(trigger, data);
        });
    };
    return InterfaceEventEmitter;
}());
var InterfaceElement = (function (_super) {
    __extends(InterfaceElement, _super);
    function InterfaceElement(id) {
        _super.call(this);
        this.addChild = function (child) {
            this.children.push(child);
            child.parent = this;
        };
        this.on = function (trigger, data) {
        };
        this.id = id;
    }
    return InterfaceElement;
}(InterfaceEventEmitter));
exports.__esModule = true;
exports["default"] = InterfaceElement;
//# sourceMappingURL=InterfaceElement.js.map
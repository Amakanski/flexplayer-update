#!/bin/bash

# These actions will be undertaken after an update

# Copy latest flexplayer build to work folder
cp -f -R /home/pi/flexplayer-update/built/ /home/pi

# Copy webapp
mkdir /home/pi/webapp
cp -f -R /home/pi/flexplayer-update/webapp/ /home/pi

# Copy scripts, flexplayer executables
cp -f /home/pi/flexplayer-update/*.sh /home/pi/
cp -f /home/pi/flexplayer-update/interface.json /home/pi/
cp -f /home/pi/flexplayer-update/settings.json /home/pi/
cp -f /home/pi/flexplayer-update/pngview /home/pi/
cp -f /home/pi/flexplayer-update/splash.png /home/pi/
cp -f /home/pi/flexplayer-update/.profile /home/pi/

# Make executables, executable
chmod a+x /home/pi/pngview

# Linux config updates, might need an additional reboot to take effect

# Disabling screensaver
sudo cp /home/pi/flexplayer-update/etc/kbd/config /etc/kbd/
sudo sh -c "TERM=linux setterm -blank 0 > /dev/tty0"

# Updating splashscreen at boot
sudo cp /home/pi/flexplayer-update/etc/init.d/asplashscreen /etc/init.d/